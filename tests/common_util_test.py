# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         common_util_test.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/8/4 9:23
# -------------------------------------------------------------------------------
from utils.common import get_files_of_dir

if __name__ == '__main__':
    # file_list = get_files(r'D:\PycharmProjects\QAPlatform', ['.git', '.py'], True)
    file_list = get_files_of_dir(r'D:\PycharmProjects\QAPlatform\utils', ['.git', '.py'])
    file_list.sort(reverse=False)
    print(file_list)
