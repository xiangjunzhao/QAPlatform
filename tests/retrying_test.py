# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         retrying_test.py
# Description:  retry测试
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/7/31 15:30
# -------------------------------------------------------------------------------

import time

from retrying import retry


class RetryClient(object):
    @retry(stop_max_attempt_number=3, wait_random_min=1000, wait_random_max=5000)
    def sum_retry(self, a, b):
        print(time.time())
        print(a + b)
        try:
            1 / 0
        except Exception as e:
            raise Exception('除数为0')


if __name__ == '__main__':
    RetryClient().sum_retry(1, 2)
