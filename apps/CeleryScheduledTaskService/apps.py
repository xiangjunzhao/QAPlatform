from django.apps import AppConfig


class CeleryScheduledTaskServiceConfig(AppConfig):
    name = 'apps.CeleryScheduledTaskService'
    verbose_name = 'Celery定时任务服务'
