# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         crontab_schedule
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------
from django_celery_beat.models import CrontabSchedule
from rest_framework.viewsets import ModelViewSet

from apps.CeleryScheduledTaskService.serializers import CrontabScheduleSerializer


class CrontabScheduleViewSet(ModelViewSet):
    queryset = CrontabSchedule.objects.all()
    serializer_class = CrontabScheduleSerializer
    ordering = ['id']
