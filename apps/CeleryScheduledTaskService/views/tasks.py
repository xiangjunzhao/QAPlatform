# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         tasks.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/11 17:26
# -------------------------------------------------------------------------------

from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from QAPlatform.celery import app as celery_app


# 获取已注册的celery任务
class TasksAPIView(GenericAPIView):
    pagination_class = None

    def get(self, request, *args, **kwargs):
        tasks = {}
        for task in celery_app.tasks:
            if not task.startswith('celery.'):
                tasks.update({task: task.split(':')[0]})
        return Response(tasks)
