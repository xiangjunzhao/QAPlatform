# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         periodic_task
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------
from django.db.models import Q
from django_celery_beat.models import PeriodicTask
from rest_framework.viewsets import ModelViewSet
from apps.CeleryScheduledTaskService.serializers import PeriodicTaskSerializer


class PeriodicTaskViewSet(ModelViewSet):
    queryset = PeriodicTask.objects.filter(~Q(task='celery.backend_cleanup'))
    serializer_class = PeriodicTaskSerializer
    ordering = ['id']
