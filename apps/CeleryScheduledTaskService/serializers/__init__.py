# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         __init__.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------
from apps.CeleryScheduledTaskService.serializers.crontab_schedule import CrontabScheduleSerializer
from apps.CeleryScheduledTaskService.serializers.periodic_task import PeriodicTaskSerializer
