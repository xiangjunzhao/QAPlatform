# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         crontab_schedule
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from django_celery_beat.models import CrontabSchedule
from rest_framework import serializers


class CrontabScheduleSerializer(serializers.ModelSerializer):
    timezone = serializers.CharField(default='Asia/Shanghai', max_length=63)

    class Meta:
        model = CrontabSchedule
        fields = '__all__'
