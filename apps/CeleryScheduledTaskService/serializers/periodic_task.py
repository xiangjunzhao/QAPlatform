# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         periodic_task
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from django_celery_beat.models import PeriodicTask
from rest_framework import serializers

from apps.CeleryScheduledTaskService.serializers import CrontabScheduleSerializer


class PeriodicTaskSerializer(serializers.ModelSerializer):
    crontab_id = serializers.CharField(write_only=True)
    crontab = CrontabScheduleSerializer(read_only=True)
    start_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    expires = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = PeriodicTask
        fields = '__all__'
