# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         urls
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from django.conf.urls import url
from rest_framework import routers

from apps.CeleryScheduledTaskService.views import *

celery_scheduled_task_service_router = routers.DefaultRouter()
celery_scheduled_task_service_router.register(prefix=r'CeleryScheduledTaskService/crontab_schedule',
                                              viewset=CrontabScheduleViewSet, basename='CeleryScheduledTaskService')
celery_scheduled_task_service_router.register(prefix=r'CeleryScheduledTaskService/periodic_task',
                                              viewset=PeriodicTaskViewSet, basename='CeleryScheduledTaskService')

urlpatterns = [
    url(r'CeleryScheduledTaskService/tasks/registered/', TasksAPIView.as_view()),
] + celery_scheduled_task_service_router.get_urls()
