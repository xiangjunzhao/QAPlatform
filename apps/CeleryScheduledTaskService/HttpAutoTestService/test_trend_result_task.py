# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         test_trend_result_task.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/2/6 上午11:01
# -------------------------------------------------------------------------------
from celery import shared_task
from django.db import connection
from django.utils import timezone

from apps.HttpAutoTestService.models import Project, TestTrendResult, Api, TestcaseResult


@shared_task(name="保存测试趋势结果:HttpAutoTestService.test_trend_result_task.save_test_trend_result")
def save_test_trend_result():
    """
    保存项目的测试趋势结果
    Returns:

    """
    projects = Project.objects.filter(is_deleted=False)
    yesterday = timezone.localdate() + timezone.timedelta(days=-1)
    test_trend_result_list = []
    for project in projects:
        project_id = project.id
        if not TestTrendResult.objects.filter(execute_date=yesterday, project_id=project_id).first():
            test_trend_result = {
                'project_id': project_id,
                'execute_date': yesterday,
                'pass_testcase_count': 0,
                'fail_testcase_count': 0,
                'total_testcase_count': 0,
                'total_api_count': 0,
                'covered_api_count': 0
            }
            sql = f"""
                SELECT t1.status, COUNT(t1.status)
                FROM http_testcase_result AS t1
                         RIGHT JOIN (SELECT testcase_id, MAX(execute_time) AS max_execute_time
                                     FROM http_testcase_result
                                     WHERE DATE (execute_time) = "{yesterday.strftime('%Y-%m-%d')}"
                                       AND project_id = "{project_id}"
                                       AND is_deleted = false
                                     GROUP BY testcase_id) AS t2
                                    ON t1.testcase_id = t2.testcase_id AND t1.execute_time = t2.max_execute_time
                GROUP BY t1.status;
            """
            with connection.cursor() as cursor:
                cursor.execute(sql)
                sql_result = cursor.fetchall()
                for item in sql_result:
                    if item[0] == 'PASS':
                        test_trend_result.update({'pass_testcase_count': item[1]})
                    else:
                        test_trend_result.update({'fail_testcase_count': item[1]})
                test_trend_result['total_testcase_count'] += test_trend_result['pass_testcase_count']
                test_trend_result['total_testcase_count'] += test_trend_result['fail_testcase_count']

            total_api_count = Api.objects.filter(is_deleted=False, project_id=project_id).count()
            test_trend_result['total_api_count'] = total_api_count

            covered_api = TestcaseResult.objects.filter(is_deleted=False, project_id=project_id,
                                                        execute_time__date=yesterday).values_list('api_id', flat=False)
            covered_api_count = len(set(covered_api))
            test_trend_result['covered_api_count'] = covered_api_count

            test_trend_result = TestTrendResult(**test_trend_result)
            test_trend_result_list.append(test_trend_result)
    TestTrendResult.objects.bulk_create(test_trend_result_list)
