# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         signals
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver

User = get_user_model()


@receiver(post_save, sender=User)
def create_user(sender, instance=None, created=False, **kwargs):
    if created:
        raw_password = instance.password if instance.password else settings.DEFAULT_PASSWD
        instance.set_password(raw_password)
        instance.save()
