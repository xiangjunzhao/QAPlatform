# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         password.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/26 16:08
# -------------------------------------------------------------------------------
from rest_framework.mixins import UpdateModelMixin
from rest_framework.viewsets import GenericViewSet
from apps.BasicAuthService.models import User
from apps.BasicAuthService.serializers import PwdSerializer

__all__ = ['PwdViewSet']


class PwdViewSet(UpdateModelMixin, GenericViewSet):
    def get_queryset(self):
        return User.objects.filter(id=self.request.data['id'])

    serializer_class = PwdSerializer
