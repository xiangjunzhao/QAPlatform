# coding: utf-8

from apps.BasicAuthService.views.permission import *
from apps.BasicAuthService.views.role import *
from apps.BasicAuthService.views.user import *
from apps.BasicAuthService.views.password import *
from apps.BasicAuthService.views.markdown import *
