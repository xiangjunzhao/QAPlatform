# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         markdown
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020-02-10
# -------------------------------------------------------------------------------

__all__ = ['MarkdownViewSet']

from rest_framework.viewsets import ModelViewSet

from apps.BasicAuthService.filters import MarkdownFilter
from apps.BasicAuthService.models import Markdown
from apps.BasicAuthService.serializers import MarkdownSerializer


class MarkdownViewSet(ModelViewSet):
    queryset = Markdown.objects.filter(is_deleted=False)
    serializer_class = MarkdownSerializer
    filter_class = MarkdownFilter
