# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         user
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------
from rest_framework import status
from rest_framework.mixins import UpdateModelMixin, DestroyModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet

from QAPlatform.mixins import CustomCreateModelMixin
from apps.BasicAuthService.filters import UserFilter
from apps.BasicAuthService.models import User
from apps.BasicAuthService.serializers import UserSerializer, UserListSerializer, UserNameListSerializer

__all__ = ['UserNameListViewSet', 'UserListViewSet', 'UserViewSet']


class UserNameListViewSet(ReadOnlyModelViewSet):
    """
    用户名称列表
    """
    queryset = User.objects.filter(is_deleted=False).values('id', 'username', 'real_name')
    serializer_class = UserNameListSerializer
    pagination_class = None


class UserListViewSet(ReadOnlyModelViewSet):
    queryset = User.objects.filter(is_deleted=False)
    serializer_class = UserListSerializer
    filter_class = UserFilter


class UserViewSet(CustomCreateModelMixin, UpdateModelMixin, DestroyModelMixin, GenericViewSet):
    queryset = User.objects.filter(is_deleted=False)
    serializer_class = UserSerializer

    def destroy(self, request, *args, **kwargs):
        id = kwargs.get('pk')
        user = User.objects.get(id=id)
        user.roles.clear()
        user.permissions.clear()
        user.is_deleted = True
        user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
