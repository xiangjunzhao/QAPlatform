# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         user
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

import django_filters

from apps.BasicAuthService.models import User

__all__ = ['UserFilter']


class UserFilter(django_filters.rest_framework.FilterSet):
    """
    用户过滤
    """
    username = django_filters.CharFilter(field_name='username', lookup_expr='icontains')
    real_name = django_filters.CharFilter(field_name='real_name', lookup_expr='icontains')
    phone = django_filters.CharFilter(field_name='phone', lookup_expr='icontains')

    class Meta:
        model = User
        fields = ['username', 'real_name', 'phone']
