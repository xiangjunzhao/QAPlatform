# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         markdown
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020-02-10
# -------------------------------------------------------------------------------

import django_filters

from apps.BasicAuthService.models import Markdown

__all__ = ['MarkdownFilter']


class MarkdownFilter(django_filters.rest_framework.FilterSet):
    """
    Markdown过滤
    """
    types = django_filters.CharFilter(field_name='types', lookup_expr='exact')

    class Meta:
        model = Markdown
        fields = ['types']
