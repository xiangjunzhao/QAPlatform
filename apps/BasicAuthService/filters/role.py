# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         role
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

import django_filters

from apps.BasicAuthService.models import Role

__all__ = ['RoleFilter']


class RoleFilter(django_filters.rest_framework.FilterSet):
    """
    角色过滤
    """
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')
    code = django_filters.CharFilter(field_name='code', lookup_expr='icontains')

    class Meta:
        model = Role
        fields = ['name', 'code']
