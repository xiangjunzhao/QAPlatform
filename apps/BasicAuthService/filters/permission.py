# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         permission
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

import django_filters

from apps.BasicAuthService.models import Permission

__all__ = ['PermissionFilter']


class PermissionFilter(django_filters.rest_framework.FilterSet):
    """
    权限过滤
    """
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')
    code = django_filters.CharFilter(field_name='code', lookup_expr='icontains')
    types = django_filters.CharFilter(field_name='types', lookup_expr='exact')

    class Meta:
        model = Permission
        fields = ['name', 'code', 'types']
