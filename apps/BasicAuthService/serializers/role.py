# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         role
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from rest_framework import serializers
from apps.BasicAuthService.models import Role
from apps.BasicAuthService.serializers import BaseSerializer, BaseListSerializer

__all__ = ['RoleListSerializer', 'RoleSerializer']


class RoleListSerializer(BaseListSerializer):
    permissions = serializers.SerializerMethodField()

    def get_permissions(self, obj):
        permissions = obj.permissions.filter(is_deleted=False)
        return [item.id for item in permissions] if permissions else []

    class Meta:
        model = Role
        fields = '__all__'


class RoleSerializer(BaseSerializer):
    class Meta:
        model = Role
        fields = '__all__'
