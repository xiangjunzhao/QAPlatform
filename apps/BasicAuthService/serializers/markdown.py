# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         markdown
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020-02-10
# -------------------------------------------------------------------------------

from apps.BasicAuthService.models import Markdown
from apps.BasicAuthService.serializers import BaseSerializer

__all__ = ['MarkdownSerializer']


class MarkdownSerializer(BaseSerializer):
    def create(self, validated_data):
        validated_data['creator'] = self.context['request'].user
        validated_data['modifier'] = self.context['request'].user
        return Markdown.objects.create(**validated_data)

    class Meta:
        model = Markdown
        fields = ['id', 'name', 'types', 'content']
