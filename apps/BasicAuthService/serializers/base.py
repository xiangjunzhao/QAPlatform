# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         base.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-17
# -------------------------------------------------------------------------------

from rest_framework import serializers

from apps.BasicAuthService.models import User

__all__ = ['BaseSerializer', 'BaseListSerializer', 'TidyUserSerializer']


class TidyUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'real_name', 'is_deleted')


class BaseSerializer(serializers.ModelSerializer):
    modifier = serializers.HiddenField(default=serializers.CurrentUserDefault())


class BaseListSerializer(serializers.ModelSerializer):
    create_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    update_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    modifier = TidyUserSerializer()
