# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         permission
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from rest_framework import serializers
from apps.BasicAuthService.models import Permission
from apps.BasicAuthService.serializers import BaseSerializer, BaseListSerializer

__all__ = ['PermissionNameListSerializer', 'PermissionListSerializer', 'PermissionSerializer']


class PermissionNameListSerializer(serializers.ModelSerializer):
    parent = serializers.CharField(read_only=True)

    class Meta:
        model = Permission
        fields = ('id', 'name', 'parent')


class PermissionListSerializer(BaseListSerializer):
    class Meta:
        model = Permission
        fields = '__all__'


class PermissionSerializer(BaseSerializer):
    class Meta:
        model = Permission
        fields = '__all__'
