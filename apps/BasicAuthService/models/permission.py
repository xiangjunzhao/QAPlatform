# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         permission
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from django.db import models
from apps.BasicAuthService.models import BaseModel

__all__ = ['Permission']


class Permission(BaseModel):
    name = models.CharField(null=False, blank=False, max_length=32, unique=True, verbose_name='名称')
    code = models.CharField(null=False, blank=False, max_length=32, unique=True, verbose_name='目录名称/功能权限CODE')
    types_choice = (
        ('MENU', '目录访问权限'),
        ('FUNCTION', '功能权限')
    )
    types = models.CharField(choices=types_choice, default='MENU', null=False, blank=False, max_length=32,
                             verbose_name='权限类型')
    active_choices = (
        (True, '启用'),
        (False, '禁用')
    )
    is_active = models.BooleanField(choices=active_choices, default=True, null=False, blank=False, verbose_name='启用/禁用')
    is_system = models.BooleanField(null=False, blank=False, default=False, verbose_name='是否系统内置')
    parent = models.ForeignKey(to='self', null=True, blank=True, on_delete=models.CASCADE, verbose_name='功能所属目录')
    sort = models.IntegerField(null=False, blank=False, verbose_name='排序')

    class Meta:
        verbose_name = '权限'
        verbose_name_plural = verbose_name
        db_table = 'basic_permission'
        ordering = ('sort',)

    def __str__(self):
        return self.name
