# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         user
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from django.db import models
from django.contrib.auth.models import AbstractUser
from apps.BasicAuthService.models import BaseModel

__all__ = ['User']


class User(BaseModel, AbstractUser):
    phone = models.CharField(null=False, blank=False, max_length=11, unique=True, verbose_name='联系电话')
    real_name = models.CharField(null=True, blank=True, max_length=32, unique=False, verbose_name='真实姓名')
    gender_choice = (
        ('male', '男'),
        ('female', '女')
    )
    gender = models.CharField(null=False, blank=False, choices=gender_choice, default='male', max_length=6,
                              verbose_name='性别')
    is_system = models.BooleanField(null=False, blank=False, default=False, verbose_name='是否系统内置')
    roles = models.ManyToManyField(blank=True, to='Role', related_name='user', verbose_name='角色')
    permissions = models.ManyToManyField(blank=True, to='Permission', related_name='user', verbose_name='权限')

    class Meta:
        verbose_name = '用户'
        verbose_name_plural = verbose_name
        db_table = 'basic_user'
        ordering = ('username',)

    def __str__(self):
        return self.username
