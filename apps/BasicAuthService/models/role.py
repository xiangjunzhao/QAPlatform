# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         role
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from django.db import models
from apps.BasicAuthService.models import BaseModel

__all__ = ['Role']


class Role(BaseModel):
    name = models.CharField(null=False, blank=False, max_length=32, unique=True, verbose_name='名称')
    code = models.CharField(null=False, blank=False, max_length=32, unique=True, verbose_name='角色CODE')
    permissions = models.ManyToManyField(blank=True, to='Permission', related_name='role', verbose_name='权限')
    active_choices = (
        (True, '启用'),
        (False, '禁用')
    )
    is_active = models.BooleanField(null=False, blank=False, choices=active_choices, default=True, verbose_name='启用/禁用')
    is_system = models.BooleanField(null=False, blank=False, default=False, verbose_name='是否系统内置')

    class Meta:
        verbose_name = '角色'
        verbose_name_plural = verbose_name
        db_table = 'basic_role'
        ordering = ('name',)

    def __str__(self):
        return self.name
