# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         urls
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from django.conf.urls import url
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from apps.BasicAuthService.views import *

basic_auth_service_router = routers.DefaultRouter()
basic_auth_service_router.register(prefix=r'BasicAuthService/users', viewset=UserListViewSet,
                                   basename='BasicAuthService')
basic_auth_service_router.register(prefix=r'BasicAuthService/user/names', viewset=UserNameListViewSet,
                                   basename='BasicAuthService')
basic_auth_service_router.register(prefix=r'BasicAuthService/user', viewset=UserViewSet, basename='BasicAuthService')
basic_auth_service_router.register(prefix=r'BasicAuthService/pwd', viewset=PwdViewSet,
                                   basename='BasicAuthService')
basic_auth_service_router.register(prefix=r'BasicAuthService/roles', viewset=RoleListViewSet,
                                   basename='BasicAuthService')
basic_auth_service_router.register(prefix=r'BasicAuthService/role', viewset=RoleViewSet, basename='BasicAuthService')
basic_auth_service_router.register(prefix=r'BasicAuthService/permissions', viewset=PermissionListViewSet,
                                   basename='BasicAuthService')
basic_auth_service_router.register(prefix=r'BasicAuthService/permission/names', viewset=PermissionNameListViewSet,
                                   basename='BasicAuthService')
basic_auth_service_router.register(prefix=r'BasicAuthService/permission', viewset=PermissionViewSet,
                                   basename='BasicAuthService')
basic_auth_service_router.register(prefix=r'BasicAuthService/markdown', viewset=MarkdownViewSet,
                                   basename='BasicAuthService')

urlpatterns = [
                  url(r'BasicAuthService/token$', TokenObtainPairView.as_view()),
                  url(r'BasicAuthService/token/refresh$', TokenRefreshView.as_view()),
              ] + basic_auth_service_router.get_urls()
