from django.apps import AppConfig


class BasicAuthServiceConfig(AppConfig):
    name = 'apps.BasicAuthService'
    verbose_name = '基础认证服务'

    def ready(self):
        import apps.BasicAuthService.signals