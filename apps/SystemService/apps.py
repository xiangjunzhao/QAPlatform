from django.apps import AppConfig


class SystemServiceConfig(AppConfig):
    name = 'apps.SystemService'
    verbose_name = '系统服务'
