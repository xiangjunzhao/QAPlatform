# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         base.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/1/16 下午6:00
# -------------------------------------------------------------------------------
from django.db import models

from apps.BasicAuthService.models import User
from apps.BasicAuthService.models.base import BaseModel

__all__ = ['SystemBaseModel']


class SystemBaseModel(BaseModel):
    name = models.CharField(null=False, blank=False, max_length=128, verbose_name='名称')
    creator = models.ForeignKey(to=User, related_name='+', on_delete=models.SET_NULL, null=True, blank=True,
                                verbose_name='创建人')
    modifier = models.ForeignKey(to=User, related_name='+', on_delete=models.SET_NULL, null=True, blank=True,
                                 verbose_name='修改人')

    class Meta:
        abstract = True
