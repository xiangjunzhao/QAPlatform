# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         parameter.py
# Description:  系统参数
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/10/26 16:37
# -------------------------------------------------------------------------------
from django.db import models

from apps.SystemService.models import SystemBaseModel

__all__ = ['Parameter']


class Parameter(SystemBaseModel):
    name = models.CharField(null=False, blank=False, max_length=32, unique=True, verbose_name='名称')
    key_choice = (
        ('MYSQL_CONFIG', 'MYSQL数据库配置'),
        ('BAIDU_OCR', '百度OCR'),
        ('DINGTALK', '钉钉'),
        ('FEISHU', '飞书'),
        ('QIWEI', '企微'),
        ('EMAIL', '邮件服务'),
        ('REDIS_CONFIG', 'Redis数据库配置'),
        ('REPORT', '测试报告'),
    )
    key = models.CharField(null=False, blank=False, max_length=32, unique=True, choices=key_choice,
                           verbose_name='参数KEY')
    value = models.JSONField(null=True, blank=True, verbose_name='参数VALUE')

    class Meta:
        verbose_name = '系统参数'
        verbose_name_plural = verbose_name
        db_table = 'system_parameter'
        ordering = ('name',)

    def __str__(self):
        return self.name
