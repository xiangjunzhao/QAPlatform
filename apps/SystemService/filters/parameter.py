# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         parameter.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/10/26 16:47
# -------------------------------------------------------------------------------
import django_filters

from apps.SystemService.models import Parameter

__all__ = ['ParameterFilter']


class ParameterFilter(django_filters.rest_framework.FilterSet):
    """
    系统参数过滤
    """
    key = django_filters.CharFilter(field_name='key', lookup_expr='exact')

    class Meta:
        model = Parameter
        fields = ['key']
