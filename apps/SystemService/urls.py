# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         urls.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/1/15 15:52
# -------------------------------------------------------------------------------
from django.conf.urls import url
from rest_framework import routers

from apps.SystemService.views import *

system_service_router = routers.DefaultRouter()
system_service_router.register(prefix=r'SystemService/parameter', viewset=ParameterViewSet,
                               basename='SystemService')

urlpatterns = [
    url(r'SystemService/logkit_pro/', LogkitProAPIView.as_view()),
] + system_service_router.get_urls()
