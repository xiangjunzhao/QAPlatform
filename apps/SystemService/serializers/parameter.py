# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         parameter.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/10/26 16:44
# -------------------------------------------------------------------------------
from apps.SystemService.models import Parameter
from apps.BasicAuthService.serializers import BaseSerializer

__all__ = ['ParameterSerializer']


class ParameterSerializer(BaseSerializer):
    def create(self, validated_data):
        validated_data['creator'] = self.context['request'].user
        validated_data['modifier'] = self.context['request'].user
        return Parameter.objects.create(**validated_data)

    class Meta:
        model = Parameter
        fields = ['id', 'name', 'key', 'value']
