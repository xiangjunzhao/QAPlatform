# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         parameter.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/10/26 16:50
# -------------------------------------------------------------------------------
from rest_framework.viewsets import ModelViewSet

from apps.SystemService.filters import ParameterFilter
from apps.SystemService.models import Parameter
from apps.SystemService.serializers import ParameterSerializer

__all__ = ['ParameterViewSet']


class ParameterViewSet(ModelViewSet):
    queryset = Parameter.objects.filter(is_deleted=False)
    serializer_class = ParameterSerializer
    filter_class = ParameterFilter
