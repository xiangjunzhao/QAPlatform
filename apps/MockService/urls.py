# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         urls.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/9/17 16:38
# -------------------------------------------------------------------------------
from django.conf.urls import url
from rest_framework import routers
from apps.MockService.views import *

mock_service_router = routers.DefaultRouter()

mock_service_router.register(prefix=r'MockService/platforms', viewset=PlatformListViewSet, basename='MockService')
mock_service_router.register(prefix=r'MockService/platform', viewset=PlatformViewSet, basename='MockService')
mock_service_router.register(prefix=r'MockService/url_mocks', viewset=UrlMockListViewSet, basename='MockService')
mock_service_router.register(prefix=r'MockService/url_mock', viewset=UrlMockViewSet, basename='MockService')

urlpatterns = [
    url(r'MockService/platform/batch_delete/', PlatformDestroyAPIView.as_view()),
] + mock_service_router.get_urls()
