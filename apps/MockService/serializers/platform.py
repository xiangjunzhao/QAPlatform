# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         platform.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 10:53
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.BasicAuthService.serializers import BaseListSerializer, BaseSerializer
from apps.MockService.models import Platform

__all__ = ['PlatformListSerializer', 'PlatformSerializer', 'TidyPlatformSerializer']


class PlatformListSerializer(BaseListSerializer):
    class Meta:
        model = Platform
        fields = '__all__'


class PlatformSerializer(BaseSerializer):
    def create(self, validated_data):
        validated_data['creator'] = self.context['request'].user
        validated_data['modifier'] = self.context['request'].user
        platform = Platform.objects.create(**validated_data)
        return platform

    class Meta:
        model = Platform
        fields = '__all__'


class TidyPlatformSerializer(serializers.ModelSerializer):
    class Meta:
        model = Platform
        fields = ('id', 'name', 'is_deleted')
