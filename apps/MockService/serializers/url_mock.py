# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         url_mock.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:32
# -------------------------------------------------------------------------------
from apps.BasicAuthService.serializers import BaseListSerializer, BaseSerializer
from apps.MockService.models import UrlMock
from apps.MockService.serializers import TidyPlatformSerializer

__all__ = ['UrlMockListSerializer', 'UrlMockSerializer']


class UrlMockListSerializer(BaseListSerializer):
    platform = TidyPlatformSerializer()

    class Meta:
        model = UrlMock
        fields = '__all__'


class UrlMockSerializer(BaseSerializer):
    class Meta:
        model = UrlMock
        fields = '__all__'
        extra_kwargs = {
            'platform': {'required': False},
        }
