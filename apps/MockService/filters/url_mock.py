# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         url_mock.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:32
# -------------------------------------------------------------------------------

import django_filters

from apps.MockService.models import UrlMock

__all__ = ['UrlMockFilter']


class UrlMockFilter(django_filters.rest_framework.FilterSet):
    """
    UrlMock列表过滤
    """
    platform = django_filters.CharFilter(field_name='platform__id', lookup_expr='exact')
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')
    url = django_filters.CharFilter(field_name='url', lookup_expr='icontains')

    class Meta:
        model = UrlMock
        fields = ['platform', 'name', 'url']
