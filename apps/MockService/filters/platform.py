# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         platform.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 10:58
# -------------------------------------------------------------------------------
import django_filters

from apps.MockService.models import Platform

__all__ = ['PlatformFilter']


class PlatformFilter(django_filters.rest_framework.FilterSet):
    """
    平台过滤
    """
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Platform
        fields = ['name']
