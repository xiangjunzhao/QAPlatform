# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         common.py
# Description:  公共MockAPIView
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/10/19 18:23
# -------------------------------------------------------------------------------
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView


class CommonMockAPIView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        """
        http get 请求
        """
        pass

    def post(self, request, *args, **kwargs):
        """
        http post 请求
        """
        return self.get(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """
        http put 请求
        """
        return self.get(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        """
        http patch 请求
        """
        return self.get(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """
        http delete 请求
        """
        return self.get(request, *args, **kwargs)

    def head(self, request, *args, **kwargs):
        """
        http head 请求
        """
        return self.get(request, *args, **kwargs)

    def options(self, request, *args, **kwargs):
        """
        http options 请求
        """
        return self.get(request, *args, **kwargs)

    def trace(self, request, *args, **kwargs):
        """
        http trace 请求
        """
        return self.get(request, *args, **kwargs)
