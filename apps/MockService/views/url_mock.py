# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         url_mock.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:32
# -------------------------------------------------------------------------------
from rest_framework.mixins import UpdateModelMixin, DestroyModelMixin
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet

from QAPlatform.mixins import CustomCreateModelMixin
from apps.MockService.filters import UrlMockFilter
from apps.MockService.models import UrlMock
from apps.MockService.serializers import UrlMockListSerializer, UrlMockSerializer

__all__ = ['UrlMockListViewSet', 'UrlMockViewSet']


class UrlMockListViewSet(ReadOnlyModelViewSet):
    """
    UrlMock列表
    """
    queryset = UrlMock.objects.filter(is_deleted=False)
    serializer_class = UrlMockListSerializer
    filter_class = UrlMockFilter


class UrlMockViewSet(CustomCreateModelMixin, UpdateModelMixin, DestroyModelMixin, GenericViewSet):
    """
    UrlMock创建、更新
    """
    queryset = UrlMock.objects.filter(is_deleted=False)
    serializer_class = UrlMockSerializer
