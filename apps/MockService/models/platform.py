# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         platform.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/9/30 11:20
# -------------------------------------------------------------------------------
from apps.MockService.models.base import MockBaseModel

__all__ = ['Platform']


class Platform(MockBaseModel):
    class Meta:
        verbose_name = '平台'
        verbose_name_plural = verbose_name
        db_table = 'mock_platform'
        ordering = ('name',)

    def __str__(self):
        return self.name
