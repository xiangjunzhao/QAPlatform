# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         url_mock.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/9/17 17:12
# -------------------------------------------------------------------------------
import json

from django.db import models

from apps.MockService.models.base import MockBaseModel

__all__ = ['UrlMock']


class UrlMock(MockBaseModel):
    url = models.CharField(max_length=512, null=False, blank=False, verbose_name='http请求URL')
    method_choice = (
        ('GET', 'GET'),
        ('PUT', 'PUT'),
        ('POST', 'POST'),
        ('PATCH', 'PATCH'),
        ('DELETE', 'DELETE'),
        ('OPTIONS', 'OPTIONS'),
        ('HEAD', 'HEAD'),
    )
    method = models.CharField(max_length=11, null=False, blank=False, default='GET', choices=method_choice,
                              verbose_name='http请求方法')
    is_check = models.BooleanField(null=False, blank=False, default=False, verbose_name='是否校验必填参数')
    check_params = models.TextField(null=True, blank=True, verbose_name='必填参数(英文逗号分隔)')
    status_code = models.IntegerField(null=True, blank=True, verbose_name='Mock状态码')
    mock_data = models.TextField(null=False, blank=False, default=json.dumps({}), verbose_name='Mock数据')
    platform = models.ForeignKey(to='Platform', related_name='api', on_delete=models.SET_NULL, null=True, blank=True,
                                 verbose_name='所属平台')

    class Meta:
        verbose_name = 'UrlMock'
        verbose_name_plural = verbose_name
        unique_together = ['url', 'method', 'platform']
        db_table = 'mock_url'
        ordering = ('name',)

    def __str__(self):
        return self.name
