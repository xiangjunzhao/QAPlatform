# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         __init__.py.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/9/17 16:35
# -------------------------------------------------------------------------------
from apps.MockService.models.platform import Platform
from apps.MockService.models.url_mock import UrlMock
