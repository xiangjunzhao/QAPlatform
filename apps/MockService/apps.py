from django.apps import AppConfig


class MockserviceConfig(AppConfig):
    name = 'apps.MockService'
    verbose_name = 'Mock服务'
