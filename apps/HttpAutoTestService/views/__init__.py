# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         __init__.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-17
# -------------------------------------------------------------------------------

from apps.HttpAutoTestService.views.environment import *
from apps.HttpAutoTestService.views.project import *
from apps.HttpAutoTestService.views.module import *
from apps.HttpAutoTestService.views.api import *
from apps.HttpAutoTestService.views.random_str import *
from apps.HttpAutoTestService.views.reports import *
from apps.HttpAutoTestService.views.testcase import *
from apps.HttpAutoTestService.views.testsuite import *
from apps.HttpAutoTestService.views.testsuite_set import *
from apps.HttpAutoTestService.views.testcase_result import *
from apps.HttpAutoTestService.views.testsuite2testcase import *
from apps.HttpAutoTestService.views.testsuite_set2testsuite import *
from apps.HttpAutoTestService.views.testsuite_result import *
from apps.HttpAutoTestService.views.ext_method import *
from apps.HttpAutoTestService.views.testcase_folder import *
from apps.HttpAutoTestService.views.test_trend_result import *
