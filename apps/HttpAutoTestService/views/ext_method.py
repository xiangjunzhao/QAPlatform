# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         ext_method.py
# Description:  扩展方法视图
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/13 下午6:36
# -------------------------------------------------------------------------------
from rest_framework import status
from rest_framework.mixins import UpdateModelMixin, DestroyModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet

from QAPlatform.mixins import CustomCreateModelMixin
from apps.HttpAutoTestService.filters import ExtMethodFilter
from apps.HttpAutoTestService.models import ExtMethod
from apps.HttpAutoTestService.serializers import ExtMethodListSerializer, ExtMethodSerializer, \
    ExtMethodNameListSerializer

__all__ = ['ExtMethodNameListViewSet', 'ExtMethodListViewSet', 'ExtMethodViewSet']


class ExtMethodNameListViewSet(ReadOnlyModelViewSet):
    """
    扩展方法名称列表
    """
    queryset = ExtMethod.objects.filter(is_deleted=False)
    serializer_class = ExtMethodNameListSerializer
    filter_class = ExtMethodFilter
    pagination_class = None


class ExtMethodListViewSet(ReadOnlyModelViewSet):
    """
    扩展方法列表
    """
    queryset = ExtMethod.objects.filter(is_deleted=False)
    serializer_class = ExtMethodListSerializer
    filter_class = ExtMethodFilter


class ExtMethodViewSet(CustomCreateModelMixin, UpdateModelMixin, DestroyModelMixin, GenericViewSet):
    """
    扩展方法创建、更新
    """
    queryset = ExtMethod.objects.filter(is_deleted=False)
    serializer_class = ExtMethodSerializer

    def destroy(self, request, *args, **kwargs):
        id = kwargs.get('pk')
        ExtMethod.objects.filter(id=id).update(is_deleted=True)
        return Response(status=status.HTTP_204_NO_CONTENT)
