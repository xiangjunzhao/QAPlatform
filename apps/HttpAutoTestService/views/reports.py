# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         reports.py
# Description:  报表
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/12/25 16:01
# -------------------------------------------------------------------------------
from django.core.paginator import Paginator
from django.db.models import Sum
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from apps.HttpAutoTestService.core.builtin.functions import ms_fmt_hms
from apps.HttpAutoTestService.models import Project, Module, Api, Testsuite, Testcase, Environment, TestcaseResult, \
    TestsuiteResult, ExtMethodResult, SQLResult
from apps.HttpAutoTestService.services.result_info_service import TestcaseResultInfoServiceImpl, \
    ExtMethodResultInfoServiceImpl, SQLResultInfoServiceImpl

__all__ = ['SummaryReportAPIView', 'ReportListAPIView', 'TestcaseReportAPIView', 'TestsuiteReportAPIView']


class SummaryReportAPIView(APIView):
    """
    接口自动化首页，全局概要统计
    """

    def get(self, request, *args, **kwargs):
        environment_count = Environment.objects.filter(is_deleted=False).count()
        project_count = Project.objects.filter(is_deleted=False).count()
        module_count = Module.objects.filter(is_deleted=False).count()
        api_count = Api.objects.filter(is_deleted=False).count()
        testsuite_count = Testsuite.objects.filter(is_deleted=False).count()
        testcase_count = Testcase.objects.filter(is_deleted=False).count()
        result = {
            'environment': environment_count,
            'project': project_count,
            'module': module_count,
            'api': api_count,
            'testsuite': testsuite_count,
            'testcase': testcase_count
        }
        return Response(data=result, content_type='application/json')


class ReportListAPIView(ListAPIView):
    """
    接口自动化首页，项目维度概要统计
    """

    def get(self, request, *args, **kwargs):
        query_params = request.query_params
        page = query_params.get('page', 1)
        page_size = query_params.get('page_size', 10)
        project_list = Project.objects.filter(is_deleted=False)
        count = len(project_list)
        project_list = Paginator(project_list, page_size).get_page(page).object_list
        results = []
        for project in project_list:
            result = {
                'project': {
                    'id': project.id,
                    'name': project.name
                },
                'environment': {
                    'total': 0
                },
                'module': {
                    'total': 0
                },
                'api': {
                    'total': 0,
                    'initial': 0,
                    'pass': 0,
                    'partial_pass': 0,
                    'fail': 0
                },
                'testsuite': {
                    'total': 0,
                    'initial': 0,
                    'pass': 0,
                    'partial_pass': 0,
                    'fail': 0
                },
                'testcase': {
                    'total': 0,
                    'initial': 0,
                    'pass': 0,
                    'fail': 0
                }
            }
            result['environment']['total'] = project.environments.filter(is_deleted=False).count()
            result['module']['total'] = project.module.filter(is_deleted=False).count()
            api_list = project.api.filter(is_deleted=False)
            result['api']['total'] = api_list.count()
            for api in api_list:
                testcase_total = api.testcase.filter(is_deleted=False).count()
                testcase_initial_total = api.testcase.filter(status='INITIAL', is_deleted=False).count()
                testcase_pass_total = api.testcase.filter(status='PASS', is_deleted=False).count()
                testcase_fail_total = api.testcase.filter(status='FAIL', is_deleted=False).count()
                result['testcase']['total'] += testcase_total
                result['testcase']['initial'] += testcase_initial_total
                result['testcase']['pass'] += testcase_pass_total
                result['testcase']['fail'] += testcase_fail_total

                if testcase_total == 0 or testcase_initial_total == testcase_total:
                    result['api']['initial'] += 1
                if testcase_total > 0:
                    if testcase_pass_total == testcase_total:
                        result['api']['pass'] += 1
                    elif testcase_fail_total == testcase_total:
                        result['api']['fail'] += 1
                    elif 0 < testcase_pass_total < testcase_total:
                        result['api']['partial_pass'] += 1

            testsuite_list = project.testsuite.filter(is_deleted=False)
            result['testsuite']['total'] = testsuite_list.count()
            result['testsuite']['initial'] = testsuite_list.filter(status='INITIAL').count()
            result['testsuite']['pass'] = testsuite_list.filter(status='PASS').count()
            result['testsuite']['fail'] = testsuite_list.filter(status='FAIL').count()
            result['testsuite']['partial_pass'] = testsuite_list.filter(status='PARTIAL_PASS').count()

            results.append(result)
        return Response(data={'count': count, 'results': results}, content_type='application/json')


# @csrf_exempt
class TestcaseReportAPIView(APIView):
    """
    用例测试报告
    """
    authentication_classes = []  # 使用空列表告诉DRF不进行认证
    permission_classes = [AllowAny]  # 如果需要权限控制，可以选择性地添加权限类

    def get(self, request, *args, **kwargs):
        query_params = self.request.query_params
        # 版本号
        version = query_params.get('version')
        # 用例状态
        status = query_params.get('status', None)
        response_data = {}
        if version:
            testcase_results, total, success, fail, total_elapsed_ms = TestcaseResultInfoServiceImpl().result_info(
                version)
            # 结果成功数百分比
            try:
                success_pct = round((success / total) * 100, 2)
            except ZeroDivisionError:
                success_pct = 0
            # 根据用例状态过滤用例结果
            if status:
                testcase_results = testcase_results.filter(status=status).order_by('execute_time')
            results = []
            for item in testcase_results:
                testcase_result = {
                    'testcase_name': item.testcase_name,
                    'project_id': item.project_id,
                    'project_name': item.project_name,
                    'api_id': item.api_id,
                    'api_name': item.api_name,
                    'url': item.url,
                    'method': item.method,
                    'headers': item.headers,
                    'request_data': item.request_data,
                    'actual_status_code': item.actual_status_code,
                    'elapsed_ms': item.elapsed_ms,
                    'actual_response_data': item.actual_response_data,
                    'output_result': item.output_result,
                    'failure_reason': item.failure_reason,
                    'status': item.status
                }
                results.append(testcase_result)
            response_data = {'total': total, 'success': success, 'fail': fail, 'success_pct': success_pct,
                             'total_elapsed_ms': total_elapsed_ms, 'results': results}
        return Response(data=response_data, content_type='application/json')


# @csrf_exempt
class TestsuiteReportAPIView(APIView):
    """
    场景测试报告
    """
    authentication_classes = []  # 使用空列表告诉DRF不进行认证
    permission_classes = [AllowAny]  # 如果需要权限控制，可以选择性地添加权限类

    def get(self, request, *args, **kwargs):
        query_params = self.request.query_params
        # 版本号
        version = query_params.get('version')
        # 场景状态
        status = query_params.get('status', None)
        response_data = {}
        if version:
            # 统计测试用例结果
            testcase_results, testcase_result_total, testcase_result_success, testcase_result_fail, total_elapsed_ms1 = \
                TestcaseResultInfoServiceImpl().result_info(version)
            # 统计扩展方法结果
            ext_method_results, ext_method_result_total, ext_method_result_success, ext_method_result_fail, total_elapsed_ms2 = \
                ExtMethodResultInfoServiceImpl().result_info(version)
            # 统计sql执行结果
            sql_results, sql_result_total, sql_result_success, sql_result_fail, total_elapsed_ms3 = \
                SQLResultInfoServiceImpl().result_info(version)

            # 测试用例结果、扩展方法结果、sql执行结果总数
            total_count = testcase_result_total + ext_method_result_total + sql_result_total
            # 测试用例结果、扩展方法结果、sql执行结果成功总数
            total_success = testcase_result_success + ext_method_result_success + sql_result_success
            # 测试用例结果、扩展方法结果、sql执行结果失败总数
            total_fail = testcase_result_fail + ext_method_result_fail + sql_result_fail
            # 结果成功数百分比
            try:
                success_pct = round((total_success / total_count) * 100, 2)
            except ZeroDivisionError:
                success_pct = 0

            # 测试场景结果
            testsuite_results = TestsuiteResult.objects.filter(version=version, is_deleted=False).order_by(
                'execute_time')
            # 测试场景执行总耗时
            total_elapsed_ms = testsuite_results.aggregate(total_elapsed_ms=Sum('elapsed_ms'))['total_elapsed_ms'] or 0
            total_elapsed_ms = ms_fmt_hms(total_elapsed_ms)
            # 根据场景状态过滤场景结果
            if status:
                testsuite_results = testsuite_results.filter(status=status).order_by('execute_time')
            results = []
            for testsuite_result in testsuite_results:
                testsuite_result_serializer = {
                    'id': testsuite_result.id,
                    'testsuite_id': testsuite_result.testsuite_id,
                    'testsuite_name': testsuite_result.testsuite_name,
                    'project_id': testsuite_result.project_id,
                    'project_name': testsuite_result.project_name,
                    'elapsed_ms': testsuite_result.elapsed_ms,
                    'status': testsuite_result.status
                }
                testsuite_result_id = testsuite_result.id

                total, success, fail = 0, 0, 0
                total += testcase_results.filter(testsuite_result_id=testsuite_result_id).count()
                total += ext_method_results.filter(testsuite_result_id=testsuite_result_id).count()
                total += sql_results.filter(testsuite_result_id=testsuite_result_id).count()
                success += testcase_results.filter(testsuite_result_id=testsuite_result_id, status='PASS').count()
                success += ext_method_results.filter(testsuite_result_id=testsuite_result_id, status='PASS').count()
                success += sql_results.filter(testsuite_result_id=testsuite_result_id, status='PASS').count()
                fail += testcase_results.filter(testsuite_result_id=testsuite_result_id, status='FAIL').count()
                fail += ext_method_results.filter(testsuite_result_id=testsuite_result_id, status='FAIL').count()
                fail += sql_results.filter(testsuite_result_id=testsuite_result_id, status='FAIL').count()
                # try:
                #     success_pct = round((success / total) * 100, 2)
                # except ZeroDivisionError:
                #     success_pct = 0
                # 场景用例结果和扩展方法结果的总数
                testsuite_result_serializer['total'] = total
                # 场景成功用例结果和成功扩展方法结果的总数
                testsuite_result_serializer['success'] = success
                # 场景失败用例结果和失败扩展方法结果的总数
                testsuite_result_serializer['fail'] = fail
                # 场景成功用例结果和成功扩展方法结果总数的百分比
                testsuite_result_serializer['success_pct'] = round((success / total) * 100, 2) if total != 0 else 0
                testsuite_result_serializer['result'] = []
                testcase_results_temp = testcase_results.filter(testsuite_result_id=testsuite_result_id).order_by(
                    'execute_time')
                ext_method_results_temp = ext_method_results.filter(testsuite_result_id=testsuite_result_id).order_by(
                    'execute_time')
                sql_results_temp = sql_results.filter(testsuite_result_id=testsuite_result_id).order_by('execute_time')
                # 合并测试用例结果和扩展方法结果
                result_list = list(testcase_results_temp) + list(ext_method_results_temp) + list(sql_results_temp)
                # 结果根据执行时间升序排列
                result_list.sort(key=lambda x: x.execute_time)
                for item in result_list:
                    if isinstance(item, TestcaseResult):
                        data = {
                            'type': 'testcase_result',
                            'testcase_name': item.testcase_name,
                            'api_name': item.api_name,
                            'url': item.url,
                            'method': item.method,
                            'headers': item.headers,
                            'request_data': item.request_data,
                            'actual_status_code': item.actual_status_code,
                            'elapsed_ms': item.elapsed_ms,
                            'actual_response_data': item.actual_response_data,
                            'output_result': item.output_result,
                            'failure_reason': item.failure_reason,
                            'status': item.status
                        }
                        testsuite_result_serializer['result'].append(data)
                    elif isinstance(item, ExtMethodResult):
                        data = {
                            'type': 'ext_method_result',
                            'testcase_name': item.ext_method_name,
                            'url': item.ext_method,
                            'elapsed_ms': item.elapsed_ms,
                            'actual_response_data': item.ext_method_run_result,
                            'output_result': item.output_result,
                            'failure_reason': item.failure_reason,
                            'status': item.status
                        }
                        testsuite_result_serializer['result'].append(data)
                    elif isinstance(item, SQLResult):
                        data = {
                            'type': 'sql_result',
                            'api_name': item.sql_ext,
                            'testcase_name': item.sql_desc,
                            'url': item.sql,
                            'elapsed_ms': item.elapsed_ms,
                            'actual_response_data': item.sql_run_result,
                            'output_result': item.output_result,
                            'failure_reason': item.failure_reason,
                            'status': item.status
                        }
                        testsuite_result_serializer['result'].append(data)
                results.append(testsuite_result_serializer)
            response_data = {'total': total_count,
                             'success': total_success,
                             'fail': total_fail,
                             'success_pct': success_pct,
                             'total_elapsed_ms': total_elapsed_ms,
                             'testcase_result_success': testcase_result_success,
                             'testcase_result_fail': testcase_result_fail,
                             'ext_method_result_success': ext_method_result_success,
                             'ext_method_result_fail': ext_method_result_fail,
                             'sql_result_success': sql_result_success,
                             'sql_result_fail': sql_result_fail,
                             'results': results}
        return Response(data=response_data, content_type='application/json')
