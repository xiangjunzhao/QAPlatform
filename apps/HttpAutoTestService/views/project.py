# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         project.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 10:55
# ------------------------------------------------------- ¬------------------------
from rest_framework import status
from rest_framework.mixins import UpdateModelMixin, DestroyModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet

from QAPlatform.mixins import CustomCreateModelMixin
from apps.BasicAuthService.models import Role
from apps.HttpAutoTestService.filters import ProjectFilter
from apps.HttpAutoTestService.models import Project, Api, Module, TestcaseFolder
from apps.HttpAutoTestService.serializers import ProjectListSerializer, ProjectSerializer, ProjectNameListSerializer

__all__ = ['ProjectNameListViewSet', 'ProjectListViewSet', 'ProjectViewSet']


class ProjectNameListViewSet(ReadOnlyModelViewSet):
    """
    项目名称列表
    """
    serializer_class = ProjectNameListSerializer
    pagination_class = None

    def get_queryset(self):
        user = self.request.user
        projects = Project.objects.filter(is_deleted=False).values('id', 'name', 'global_variables',
                                                                   'default_environment_id')
        if not Role.objects.filter(code='SUPERADMIN', user=user, is_deleted=False):
            projects = projects.filter(members__exact=user).distinct()
        return projects


class ProjectListViewSet(ReadOnlyModelViewSet):
    """
    项目列表
    """
    serializer_class = ProjectListSerializer
    filter_class = ProjectFilter

    def get_queryset(self):
        user = self.request.user
        projects = Project.objects.filter(is_deleted=False)
        if not Role.objects.filter(code='SUPERADMIN', user=user, is_deleted=False):
            projects = projects.filter(members__exact=user).distinct()
        return projects


class ProjectViewSet(CustomCreateModelMixin, UpdateModelMixin, DestroyModelMixin, GenericViewSet):
    """
    项目创建、更新
    """
    queryset = Project.objects.filter(is_deleted=False)
    serializer_class = ProjectSerializer

    def perform_create(self, serializer):
        user = self.request.user
        instance = serializer.save()
        instance.creator = user
        instance.save()
        Module(name='项目默认模块', project=instance, is_system=True, creator=user, modifier=user).save()
        TestcaseFolder(name='项目默认用例夹', project=instance, is_system=True, creator=user, modifier=user).save()

    def destroy(self, request, *args, **kwargs):
        id = kwargs.get('pk')
        apis = Api.objects.filter(project_id=id, is_deleted=False)
        if apis:
            return Response(status=status.HTTP_200_OK, data='删除失败：该项目中存在接口，不允许被删除', content_type='application/json')

        project = Project.objects.filter(id=id, is_deleted=False).first()
        if project:
            project.environments.clear()
            project.members.clear()
            project.is_deleted = True
            project.save()

        Module.objects.filter(project_id=id).update(is_deleted=True)
        return Response(status=status.HTTP_204_NO_CONTENT)
