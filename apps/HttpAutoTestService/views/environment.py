# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         environment.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-17
# -------------------------------------------------------------------------------
from rest_framework import status
from rest_framework.mixins import UpdateModelMixin, DestroyModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet

from QAPlatform.mixins import CustomCreateModelMixin
from apps.HttpAutoTestService.filters import EnvironmentFilter, EnvironmentNameFilter
from apps.HttpAutoTestService.models import Environment, Project, Testsuite
from apps.HttpAutoTestService.serializers import EnvironmentListSerializer, EnvironmentSerializer, \
    EnvironmentNameListSerializer

__all__ = ['EnvironmentNameListViewSet', 'EnvironmentListViewSet', 'EnvironmentViewSet']


class EnvironmentNameListViewSet(ReadOnlyModelViewSet):
    """
    环境名称列表
    """
    queryset = Environment.objects.filter(is_deleted=False).values('id', 'name', 'base_url')
    serializer_class = EnvironmentNameListSerializer
    filter_class = EnvironmentNameFilter
    pagination_class = None


class EnvironmentListViewSet(ReadOnlyModelViewSet):
    """
    环境列表
    """
    queryset = Environment.objects.filter(is_deleted=False)
    serializer_class = EnvironmentListSerializer
    filter_class = EnvironmentFilter


class EnvironmentViewSet(CustomCreateModelMixin, UpdateModelMixin, DestroyModelMixin, GenericViewSet):
    """
    环境创建、更新
    """
    queryset = Environment.objects.filter(is_deleted=False)
    serializer_class = EnvironmentSerializer

    def destroy(self, request, *args, **kwargs):
        id = kwargs.get('pk')
        environment = Environment.objects.filter(id=id, is_deleted=False).first()
        if environment:
            if Project.objects.filter(default_environment_id=id, is_deleted=False):
                # 当存在项目使用此环境作为默认环境时，不允许删除该环境
                return Response(status=status.HTTP_200_OK, data='删除失败：该环境已被项目设定为默认环境，不允许被删除',
                                content_type='application/json')

            if Testsuite.objects.filter(private_environment_id=id, is_deleted=False):
                # 当存在场景使用此环境作为私有环境时，不允许删除该环境
                return Response(status=status.HTTP_200_OK, data='删除失败：该环境已被测试场景设定为私有环境，不允许被删除',
                                content_type='application/json')

            environment.project.clear()
            environment.is_deleted = True
            environment.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
