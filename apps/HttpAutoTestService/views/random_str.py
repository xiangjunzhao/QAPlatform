# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         random_str.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/4/28 11:38
# -------------------------------------------------------------------------------
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from apps.HttpAutoTestService.core.builtin.functions import gen_random_str
from apps.HttpAutoTestService.models import RandomStr

__all__ = ['RandomStrAPIView']


class RandomStrAPIView(APIView):
    """
    获取随机字符串
    """

    def get(self, request, *args, **kwargs):
        length = request.data.get('length', 6)
        while True:
            random_str = gen_random_str(length)
            if not RandomStr.objects.filter(random_str=random_str):
                RandomStr(random_str=random_str).save()
                break
        return Response(status=status.HTTP_200_OK, data=random_str, content_type='application/json')
