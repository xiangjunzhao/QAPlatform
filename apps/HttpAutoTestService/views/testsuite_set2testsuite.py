# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite_set2testsuite.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/26 9:16
# -------------------------------------------------------------------------------
from rest_framework import status
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from apps.HttpAutoTestService.filters import TestsuiteSet2TestsuiteFilter
from apps.HttpAutoTestService.models import TestsuiteSet2Testsuite
from apps.HttpAutoTestService.serializers import TestsuiteSet2TestsuiteSerializer

__all__ = ['TestsuiteSet2TestsuiteViewSet', ]


class TestsuiteSet2TestsuiteViewSet(CreateModelMixin, ListModelMixin, GenericViewSet):
    queryset = TestsuiteSet2Testsuite.objects.filter()
    serializer_class = TestsuiteSet2TestsuiteSerializer
    filter_class = TestsuiteSet2TestsuiteFilter
    pagination_class = None

    def create(self, request, *args, **kwargs):
        testsuite_set2testsuite = request.data
        testsuite_set_id = testsuite_set2testsuite.get('testsuite_set_id') if testsuite_set2testsuite else None
        data = testsuite_set2testsuite.get('data') if testsuite_set2testsuite else None

        TestsuiteSet2Testsuite.objects.filter(testsuite_set_id=testsuite_set_id).delete()
        testsuite_set2testcase = [TestsuiteSet2Testsuite(**item) for item in data]
        TestsuiteSet2Testsuite.objects.bulk_create(testsuite_set2testcase)
        return Response(status=status.HTTP_201_CREATED, content_type='application/json')
