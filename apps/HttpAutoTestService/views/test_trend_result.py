# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         test_trend_result.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/1/29 9:37
# -------------------------------------------------------------------------------
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ReadOnlyModelViewSet

from apps.HttpAutoTestService.filters import TestTrendResultFilter
from apps.HttpAutoTestService.models import TestTrendResult
from apps.HttpAutoTestService.serializers import TestTrendResultSerializer

__all__ = ['TestTrendResultViewSet']


class TestTrendResultViewSet(ReadOnlyModelViewSet):
    """
    测试趋势结果列表
    """
    queryset = TestTrendResult.objects.filter()
    serializer_class = TestTrendResultSerializer
    filter_class = TestTrendResultFilter
    pagination_class = None
