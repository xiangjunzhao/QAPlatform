# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testcase_result.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/21 11:14
# -------------------------------------------------------------------------------

import django_filters

from apps.HttpAutoTestService.models import TestcaseResult

__all__ = ['TestcaseResultFilter']


class TestcaseResultFilter(django_filters.rest_framework.FilterSet):
    """
    用例结果过滤
    """
    testcase_name = django_filters.CharFilter(field_name='testcase_name', lookup_expr='icontains')
    testsuite_result = django_filters.CharFilter(method='testsuite_result_filter')
    status = django_filters.CharFilter(field_name='status', lookup_expr='exact')
    project = django_filters.CharFilter(field_name='project_id', lookup_expr='exact')
    api = django_filters.CharFilter(method='api_filter')
    version = django_filters.CharFilter(field_name='version', lookup_expr='exact')

    def testsuite_result_filter(self, queryset, name, value):
        if value == 'null':
            return queryset.filter(testsuite_result_id__isnull=True)
        return queryset.filter(testsuite_result_id=value)

    def api_filter(self, queryset, name, value):
        api_list = value.split(',')
        return queryset.filter(api_id__in=api_list)

    class Meta:
        model = TestcaseResult
        fields = ['testcase_name', 'testsuite_result', 'status', 'project', 'version']
