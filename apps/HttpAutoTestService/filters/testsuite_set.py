# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite_set.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/2/6 上午10:29
# -------------------------------------------------------------------------------

import django_filters

from apps.HttpAutoTestService.models import TestsuiteSet

__all__ = ['TestsuiteSetFilter']


class TestsuiteSetFilter(django_filters.rest_framework.FilterSet):
    """
    场景集过滤
    """
    name = django_filters.CharFilter(field_name="name", lookup_expr="icontains")
    project = django_filters.CharFilter(field_name='project_id', lookup_expr='exact')
    status = django_filters.CharFilter(field_name='status', lookup_expr='exact')
    level = django_filters.CharFilter(method='level_filter')
    creator = django_filters.CharFilter(method='creator_filter')
    testsuite = django_filters.CharFilter(method='testsuite_filter')

    def level_filter(self, queryset, name, value):
        level_list = value.split(',')
        return queryset.filter(level__in=level_list)

    def creator_filter(self, queryset, name, value):
        creator_list = value.split(',')
        return queryset.filter(creator_id__in=creator_list)

    def testsuite_filter(self, queryset, name, value):
        testsuite_list = value.split(',')
        return queryset.filter(testsuites__in=testsuite_list).distinct()

    class Meta:
        model = TestsuiteSet
        fields = ['name', 'project', 'status']
