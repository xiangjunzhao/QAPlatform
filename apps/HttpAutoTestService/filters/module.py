# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         module.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:09
# -------------------------------------------------------------------------------

import django_filters
from apps.HttpAutoTestService.models import Module

__all__ = ['ModuleNameFilter']


class ModuleNameFilter(django_filters.rest_framework.FilterSet):
    """
    模块名称过滤
    """
    project = django_filters.CharFilter(field_name='project_id', lookup_expr='exact')

    class Meta:
        model = Module
        fields = ['project']
