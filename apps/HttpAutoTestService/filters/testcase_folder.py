# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testcase_folder.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/27 下午8:25
# -------------------------------------------------------------------------------
import django_filters

from apps.HttpAutoTestService.models import TestcaseFolder

__all__ = ['TestcaseFolderFilter']


class TestcaseFolderFilter(django_filters.rest_framework.FilterSet):
    """
    用例夹过滤
    """
    project = django_filters.CharFilter(field_name='project_id', lookup_expr='exact')

    class Meta:
        model = TestcaseFolder
        fields = ['project']
