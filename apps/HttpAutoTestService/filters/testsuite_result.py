# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite_result.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/21 11:14
# -------------------------------------------------------------------------------

import django_filters

from apps.HttpAutoTestService.models import TestsuiteResult

__all__ = ['TestsuiteResultFilter']


class TestsuiteResultFilter(django_filters.rest_framework.FilterSet):
    """
    场景结果过滤
    """
    status = django_filters.CharFilter(field_name='status', lookup_expr='exact')
    testsuite_name = django_filters.CharFilter(field_name='testsuite_name', lookup_expr='icontains')
    project = django_filters.CharFilter(field_name='project_id', lookup_expr='exact')
    version = django_filters.CharFilter(field_name='version', lookup_expr='exact')

    class Meta:
        model = TestsuiteResult
        fields = ['status', 'testsuite_name', 'project', 'version']
