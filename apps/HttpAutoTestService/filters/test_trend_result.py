# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         test_trend_result.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/1/29 9:41
# -------------------------------------------------------------------------------
import django_filters
from django.utils import timezone

from apps.CeleryScheduledTaskService.HttpAutoTestService.test_trend_result_task import save_test_trend_result
from apps.HttpAutoTestService.models import TestTrendResult

__all__ = ['TestTrendResultFilter']


class TestTrendResultFilter(django_filters.rest_framework.FilterSet):
    """
    测试趋势结果过滤
    """
    project = django_filters.CharFilter(method='project_filter')

    def project_filter(self, query_set, name, value):
        start_date = timezone.localdate() + timezone.timedelta(days=-7)
        end_date = timezone.localdate() + timezone.timedelta(days=-1)
        if False:
            # 目的是为了注册任务：save_test_trend_result
            save_test_trend_result.delay()
        return query_set.filter(execute_date__range=(start_date, end_date), project_id=value)

    class Meta:
        model = TestTrendResult
        fields = ['project']
