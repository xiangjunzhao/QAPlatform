# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite_set2testsuite.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/26 9:14
# -------------------------------------------------------------------------------

import django_filters

from apps.HttpAutoTestService.models import TestsuiteSet2Testsuite

__all__ = ['TestsuiteSet2TestsuiteFilter']


class TestsuiteSet2TestsuiteFilter(django_filters.rest_framework.FilterSet):
    """
    场景集场景过滤
    """
    testsuite_set = django_filters.CharFilter(field_name='testsuite_set_id', lookup_expr='exact')

    class Meta:
        model = TestsuiteSet2Testsuite
        fields = ['testsuite_set']
