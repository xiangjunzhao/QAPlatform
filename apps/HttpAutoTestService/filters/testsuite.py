# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/20 13:58
# -------------------------------------------------------------------------------


import django_filters
from apps.HttpAutoTestService.models import Testsuite

__all__ = ['TestsuiteNameFilter', 'TestsuiteFilter']


class TestsuiteNameFilter(django_filters.rest_framework.FilterSet):
    """
    测试场景名称过滤
    """
    project = django_filters.CharFilter(field_name='project_id', lookup_expr='exact')

    class Meta:
        model = Testsuite
        fields = ['project']


class TestsuiteFilter(django_filters.rest_framework.FilterSet):
    """
    测试场景过滤
    """
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')
    project = django_filters.CharFilter(field_name='project_id', lookup_expr='exact')
    status = django_filters.CharFilter(field_name='status', lookup_expr='exact')
    level = django_filters.CharFilter(method='level_filter')
    creator = django_filters.CharFilter(method='creator_filter')
    enable_private_env = django_filters.BooleanFilter(field_name='enable_private_env', lookup_expr='exact')

    def level_filter(self, queryset, name, value):
        level_list = value.split(',')
        return queryset.filter(level__in=level_list)

    def creator_filter(self, queryset, name, value):
        creator_list = value.split(',')
        return queryset.filter(creator_id__in=creator_list)

    class Meta:
        model = Testsuite
        fields = ['name', 'project', 'status']
