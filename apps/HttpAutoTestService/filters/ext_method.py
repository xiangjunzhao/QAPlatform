# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         ext_method.py
# Description:  扩展方法过滤器
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/13 下午6:32
# -------------------------------------------------------------------------------
import django_filters

from apps.HttpAutoTestService.models import ExtMethod

__all__ = ['ExtMethodFilter']


class ExtMethodFilter(django_filters.rest_framework.FilterSet):
    """
    扩展方法列表过滤
    """
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')
    ext_method = django_filters.CharFilter(field_name='ext_method', lookup_expr='icontains')
    type = django_filters.CharFilter(field_name='type', lookup_expr='exact')

    class Meta:
        model = ExtMethod
        fields = ['name', 'ext_method', 'type']
