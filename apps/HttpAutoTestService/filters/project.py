# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         project.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 10:58
# -------------------------------------------------------------------------------
import django_filters

from apps.HttpAutoTestService.models import Project

__all__ = ['ProjectFilter']


class ProjectFilter(django_filters.rest_framework.FilterSet):
    """
    项目过滤
    """
    name = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Project
        fields = ['name']
