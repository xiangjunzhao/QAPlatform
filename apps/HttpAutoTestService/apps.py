from django.apps import AppConfig


class HttpAutoTestServiceConfig(AppConfig):
    name = 'apps.HttpAutoTestService'
    verbose_name = '接口自动化测试服务'

    def ready(self):
        pass