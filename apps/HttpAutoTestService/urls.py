# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         urls.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/19 9:33
# -------------------------------------------------------------------------------
from django.conf.urls import url
from django.urls import path
from rest_framework import routers
from apps.HttpAutoTestService.views import *

http_auto_test_service_router = routers.DefaultRouter()

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/environments', viewset=EnvironmentListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/environment/names',
                                       viewset=EnvironmentNameListViewSet, basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/environment', viewset=EnvironmentViewSet,
                                       basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/projects', viewset=ProjectListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/project/names', viewset=ProjectNameListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/project', viewset=ProjectViewSet,
                                       basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/module/names', viewset=ModuleNameListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/module', viewset=ModuleViewSet,
                                       basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/apis', viewset=ApiListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/api/names', viewset=ApiNameListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/api', viewset=ApiViewSet,
                                       basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testcases', viewset=TestcaseListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testcase', viewset=TestcaseViewSet,
                                       basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testcase_folders',
                                       viewset=TestcaseFolderListViewSet, basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testcase_folder', viewset=TestcaseFolderViewSet,
                                       basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testsuite2testcase',
                                       viewset=Testsuite2TestcaseViewSet, basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testsuite_set2testsuite',
                                       viewset=TestsuiteSet2TestsuiteViewSet, basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testsuites', viewset=TestsuiteListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testsuite/names', viewset=TestsuiteNameListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testsuite', viewset=TestsuiteViewSet,
                                       basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testsuite_sets', viewset=TestsuiteSetListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testsuite_set', viewset=TestsuiteSetViewSet,
                                       basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testcase_results',
                                       viewset=TestcaseResultListViewSet, basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testcase_result',
                                       viewset=TestcaseResultViewSet, basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/testsuite_results',
                                       viewset=TestsuiteResultListViewSet, basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/ext_method/names', viewset=ExtMethodNameListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/ext_methods', viewset=ExtMethodListViewSet,
                                       basename='HttpAutoTestService')
http_auto_test_service_router.register(prefix=r'HttpAutoTestService/ext_method', viewset=ExtMethodViewSet,
                                       basename='HttpAutoTestService')

http_auto_test_service_router.register(prefix=r'HttpAutoTestService/test_trend_result', viewset=TestTrendResultViewSet,
                                       basename='HttpAutoTestService')

urlpatterns = [
                  url(r'HttpAutoTestService/api/batch_delete/', ApiBatchDestroyAPIView.as_view()),
                  url(r'HttpAutoTestService/api/template/download/', ApiTemplateDownloadAPIView.as_view()),
                  url(r'HttpAutoTestService/api/upload/', ApiUploadAPIView.as_view()),
                  url(r'HttpAutoTestService/api/debug/', ApiDebugAPIView.as_view()),
                  url(r'HttpAutoTestService/api/import/', ApiImportAPIView.as_view()),
                  url(r'HttpAutoTestService/api/batch_create/', ApiBatchCreateAPIView.as_view()),
                  url(r'HttpAutoTestService/testcase/batch_delete/', TestcaseBatchDestroyAPIView.as_view()),
                  url(r'HttpAutoTestService/testsuite/batch_delete/', TestsuiteBatchDestroyAPIView.as_view()),
                  url(r'HttpAutoTestService/testsuite_set/batch_delete/', TestsuiteSetBatchDestroyAPIView.as_view()),
                  url(r'HttpAutoTestService/testsuite/copy/', TestsuiteCopyAPIView.as_view()),
                  path(r'HttpAutoTestService/testsuite/global_variables/<id>', TestsuiteGlobalVariablesAPIView.as_view()),
                  url(r'HttpAutoTestService/testcase_result/version/', TestcaseResultVersionAPIView.as_view()),
                  url(r'HttpAutoTestService/testcase_result/batch_delete/',
                      TestcaseResultBatchDestroyAPIView.as_view()),
                  url(r'HttpAutoTestService/testsuite_result/version/', TestsuiteResultVersionAPIView.as_view()),
                  url(r'HttpAutoTestService/testsuite_result/batch_delete/',
                      TestsuiteResultBatchDestroyAPIView.as_view()),
                  url(r'HttpAutoTestService/testcase/debug/', TestcaseDebugAPIView.as_view()),
                  url(r'HttpAutoTestService/testcase/batch_exec/', TestcaseBatchExecAPIView.as_view()),
                  url(r'HttpAutoTestService/testcase/template/download/', TestcaseTemplateDownloadAPIView.as_view()),
                  url(r'HttpAutoTestService/testcase/upload/', TestcaseUploadAPIView.as_view()),
                  url(r'HttpAutoTestService/testcase/batch_create/', TestcaseBatchCreateAPIView.as_view()),
                  url(r'HttpAutoTestService/testsuite/batch_exec/', TestsuiteBatchExecAPIView.as_view()),
                  url(r'HttpAutoTestService/testsuite_set/batch_exec/', TestsuiteSetBatchExecAPIView.as_view()),
                  url(r'HttpAutoTestService/reports/summary/', SummaryReportAPIView.as_view()),
                  url(r'HttpAutoTestService/reports/list/', ReportListAPIView.as_view()),
                  url(r'HttpAutoTestService/report/testcase/', TestcaseReportAPIView.as_view()),
                  url(r'HttpAutoTestService/report/testsuite/', TestsuiteReportAPIView.as_view()),
                  url(r'HttpAutoTestService/testsuite/export/', TestsuiteExportAPIView.as_view()),
                  url(r'HttpAutoTestService/testsuite_set/export/', TestsuiteSetExportAPIView.as_view()),
                  url(r'HttpAutoTestService/random_str/', RandomStrAPIView.as_view()),
              ] + http_auto_test_service_router.get_urls()
