# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         file_reader.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/4/13 11:07
# -------------------------------------------------------------------------------


def read_file(filename, chunk_size=512):
    """
    读取文件
    Args:
        filename: 文件名(文件绝对路径)
        chunk_size: 块大小

    Returns:

    """
    with open(file=filename, mode='rb') as f:
        while True:
            content = f.read(chunk_size)
            if content:
                yield content
            else:
                break
