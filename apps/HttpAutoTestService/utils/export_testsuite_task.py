# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         export_testsuite_task.py
# Description:  导出测试场景
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/4/13 9:04
# -------------------------------------------------------------------------------
import json

import pandas as pd


def export_testsuite(excel_writer, sheet_name, testsuite, start_row=0):
    """
    导出测试场景用例
    Args:
        excel_writer:
        sheet_name:
        testsuite:
        start_row:数据插入开始行

    Returns:

    """
    testsuite2testcase_list = testsuite.testsuite2testcase.all()
    header_key = ['name', 'type', 'url', 'method', 'headers', 'request_data_type', 'request_data', 'create_testcase',
                  'expect_result', 'remark']
    header_value = ['*接口名称(必填)', '类型(选填)\n默认值：HTTP_API', '*请求URL(必填)', '请求方法(选填)\n默认值：POST',
                    'Headers参数(选填)\n默认值：\n{"Content-Type":"application/json"}', 'Body参数类型(选填)\n默认值：Json', 'Body参数',
                    '创建用例(选填)\n默认值：YES',
                    '用例期望结果(选填)\n默认值：\n{"output":[],"validate":[{"source": "status_code", "extractor": "JSONPATH", "expression": "status_code", "comparator": "eq", "expect_value": "200"}]}',
                    '备注']
    data = [dict(zip(header_key, header_value))]
    for testsuite2testcase in testsuite2testcase_list:
        testcase_type = testsuite2testcase.type
        if testcase_type == 'HTTP_API':
            name = testsuite2testcase.testcase_name
            url = testsuite2testcase.url
            method = testsuite2testcase.api.method
            headers = json.dumps(testsuite2testcase.headers, ensure_ascii=False)
            request_data_type = testsuite2testcase.request_data_type
            request_data = json.dumps(testsuite2testcase.request_data, ensure_ascii=False)
            create_testcase = 'YES'
            expect_result = json.dumps(testsuite2testcase.expect_result, ensure_ascii=False)
            remark = ""
            data.append(
                {'name': name, 'type': testcase_type, 'url': url, 'method': method,
                 'headers': headers, 'request_data_type': request_data_type, 'request_data': request_data,
                 'create_testcase': create_testcase, 'expect_result': expect_result, 'remark': remark}
            )
        elif testcase_type == 'EXT_METHOD':
            name = testsuite2testcase.ext_method_name
            ext_method = testsuite2testcase.ext_method
            method = ''
            headers = ''
            request_data_type = ''
            request_data = ''
            create_testcase = 'YES'
            expect_result = json.dumps(testsuite2testcase.expect_result, ensure_ascii=False)
            remark = ''
            data.append(
                {'name': name, 'type': testcase_type, 'url': ext_method, 'method': method,
                 'headers': headers, 'request_data_type': request_data_type, 'request_data': request_data,
                 'create_testcase': create_testcase, 'expect_result': expect_result, 'remark': remark}
            )
        elif testcase_type == 'SQL':
            # TODO
            pass

    workbook = excel_writer.book
    valign_center_fmt = workbook.add_format({'valign': 'vcenter', 'text_wrap': True})
    align_center_fmt = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'text_wrap': True})
    df = pd.DataFrame(data=data)
    df.to_excel(excel_writer=excel_writer, sheet_name=sheet_name, header=False, index=False, startrow=start_row)
    sheet = excel_writer.sheets[sheet_name]
    sheet.set_column('A:A', width=35, cell_format=valign_center_fmt)
    sheet.set_column('B:B', width=18, cell_format=align_center_fmt)
    sheet.set_column('C:C', width=50, cell_format=valign_center_fmt)
    sheet.set_column('D:D', width=18, cell_format=align_center_fmt)
    sheet.set_column('E:E', width=50, cell_format=valign_center_fmt)
    sheet.set_column('F:F', width=18, cell_format=align_center_fmt)
    sheet.set_column('G:G', width=50, cell_format=valign_center_fmt)
    sheet.set_column('H:H', width=18, cell_format=align_center_fmt)
    sheet.set_column('I:J', width=50, cell_format=valign_center_fmt)
    sheet.set_row(row=start_row, height=None, cell_format=align_center_fmt)
