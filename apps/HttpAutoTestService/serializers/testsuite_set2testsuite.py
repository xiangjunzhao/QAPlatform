# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite_set2testsuite.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/22 下午9:59
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.HttpAutoTestService.models import TestsuiteSet2Testsuite

__all__ = ['TestsuiteSet2TestsuiteSerializer', ]

from apps.HttpAutoTestService.serializers import TestsuiteListSerializer


class TestsuiteSet2TestsuiteSerializer(serializers.ModelSerializer):
    testsuite = TestsuiteListSerializer()

    class Meta:
        model = TestsuiteSet2Testsuite
        exclude = ('id',)
