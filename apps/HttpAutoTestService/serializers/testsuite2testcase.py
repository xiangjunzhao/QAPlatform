# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite2testcase.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/26 9:12
# -------------------------------------------------------------------------------
from rest_framework import serializers
from apps.HttpAutoTestService.models import Testsuite2Testcase
from apps.HttpAutoTestService.serializers import TidyApiSerializer2

__all__ = ['Testsuite2TestcaseSerializer', ]


class Testsuite2TestcaseSerializer(serializers.ModelSerializer):
    api = TidyApiSerializer2()

    class Meta:
        model = Testsuite2Testcase
        exclude = ('id',)
