# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         ext_method.py
# Description:  扩展方法序列化
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/13 下午6:30
# -------------------------------------------------------------------------------
from apps.BasicAuthService.serializers import BaseListSerializer, BaseSerializer
from apps.HttpAutoTestService.models import ExtMethod

__all__ = ['ExtMethodNameListSerializer', 'ExtMethodListSerializer', 'ExtMethodSerializer']


class ExtMethodNameListSerializer(BaseListSerializer):
    class Meta:
        model = ExtMethod
        fields = ('id', 'name', 'ext_method', 'type')


class ExtMethodListSerializer(BaseListSerializer):
    class Meta:
        model = ExtMethod
        fields = '__all__'


class ExtMethodSerializer(BaseSerializer):
    class Meta:
        model = ExtMethod
        fields = '__all__'
