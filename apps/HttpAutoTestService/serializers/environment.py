# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         environment.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-17
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.BasicAuthService.serializers import BaseListSerializer, BaseSerializer
from apps.HttpAutoTestService.models import Environment

__all__ = ['EnvironmentNameListSerializer', 'EnvironmentListSerializer', 'EnvironmentSerializer',
           'TidyEnvironmentSerializer']


class EnvironmentNameListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Environment
        fields = ('id', 'name', 'base_url')


class EnvironmentListSerializer(BaseListSerializer):
    class Meta:
        model = Environment
        fields = '__all__'


class EnvironmentSerializer(BaseSerializer):
    class Meta:
        model = Environment
        fields = '__all__'


class TidyEnvironmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Environment
        fields = ('id', 'name', 'base_url', 'is_deleted')
