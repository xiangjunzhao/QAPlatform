# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testcase_folder.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/27 下午8:18
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.BasicAuthService.serializers import BaseSerializer
from apps.HttpAutoTestService.models import TestcaseFolder

__all__ = ['TestcaseFolderListSerializer', 'TestcaseFolderSerializer']


class TestcaseFolderListSerializer(serializers.ModelSerializer):
    parent = serializers.CharField(read_only=True)
    project = serializers.CharField(read_only=True)

    class Meta:
        model = TestcaseFolder
        fields = ('id', 'name', 'parent', 'project', 'is_system', 'remark')


class TestcaseFolderSerializer(BaseSerializer):
    class Meta:
        model = TestcaseFolder
        fields = '__all__'
        extra_kwargs = {
            'project': {'required': True}
        }
