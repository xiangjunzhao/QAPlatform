# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/20 13:53
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.BasicAuthService.serializers import BaseListSerializer, BaseSerializer
from apps.HttpAutoTestService.models import Testsuite
from apps.HttpAutoTestService.serializers import TidyProjectSerializer

__all__ = ['TestsuiteNameListSerializer', 'TestsuiteListSerializer', 'TestsuiteSerializer']


class TestsuiteNameListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Testsuite
        fields = ('id', 'name')


class TestsuiteListSerializer(BaseListSerializer):
    project = TidyProjectSerializer()
    testcases = serializers.SerializerMethodField()

    def get_testcases(self, obj):
        result = obj.testsuite2testcase.values('type', 'ext_method_name', 'testcase_name', 'sql_desc')
        return result

    class Meta:
        model = Testsuite
        fields = '__all__'


class TestsuiteSerializer(BaseSerializer):
    class Meta:
        model = Testsuite
        exclude = ('status',)
