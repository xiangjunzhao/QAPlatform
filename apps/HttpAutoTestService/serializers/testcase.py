# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testcase.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-19
# -------------------------------------------------------------------------------
from rest_framework import serializers
from apps.BasicAuthService.serializers import BaseListSerializer, BaseSerializer
from apps.HttpAutoTestService.models import Testcase
from apps.HttpAutoTestService.serializers import TidyApiSerializer

__all__ = ['TestcaseListSerializer', 'TestcaseSerializer', 'TidyTestcaseSerializer']


class TestcaseListSerializer(BaseListSerializer):
    api = TidyApiSerializer()

    class Meta:
        model = Testcase
        fields = '__all__'


class TestcaseSerializer(BaseSerializer):
    class Meta:
        model = Testcase
        exclude = ('status',)
        extra_kwargs = {
            'api': {'required': True}
        }


class TidyTestcaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Testcase
        fields = ('id', 'name', 'is_deleted')
