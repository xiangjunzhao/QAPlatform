# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         test_trend_result.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/1/29 9:35
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.HttpAutoTestService.models import TestTrendResult

__all__ = ['TestTrendResultSerializer']


class TestTrendResultSerializer(serializers.ModelSerializer):
    execute_date = serializers.DateField(format='%Y-%m-%d')

    class Meta:
        model = TestTrendResult
        fields = '__all__'
