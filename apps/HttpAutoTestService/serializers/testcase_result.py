# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testcase_result.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/21 11:14
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.HttpAutoTestService.models import TestcaseResult

__all__ = ['TestcaseResultSerializer']


class TestcaseResultSerializer(serializers.ModelSerializer):
    execute_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = TestcaseResult
        fields = '__all__'
