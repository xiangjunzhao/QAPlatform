# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite_result.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/21 11:14
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.HttpAutoTestService.models import TestsuiteResult

__all__ = ['TestsuiteResultListSerializer']


class TestsuiteResultListSerializer(serializers.ModelSerializer):
    execute_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = TestsuiteResult
        fields = '__all__'
