# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         module.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:00
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.BasicAuthService.serializers import BaseListSerializer, BaseSerializer
from apps.HttpAutoTestService.models import Module
from apps.HttpAutoTestService.serializers import TidyProjectSerializer

__all__ = ['ModuleNameListSerializer', 'ModuleListSerializer', 'ModuleSerializer', 'TidyModuleSerializer']


class ModuleNameListSerializer(serializers.ModelSerializer):
    parent = serializers.CharField(read_only=True)
    project = serializers.CharField(read_only=True)

    class Meta:
        model = Module
        fields = ('id', 'name', 'parent', 'is_system', 'project', 'remark')


class ModuleListSerializer(BaseListSerializer):
    project = TidyProjectSerializer()

    class Meta:
        model = Module
        fields = '__all__'


class ModuleSerializer(BaseSerializer):
    class Meta:
        model = Module
        fields = '__all__'
        extra_kwargs = {
            'project': {'required': True}
        }


class TidyModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ('id', 'name', 'is_deleted')
