# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite_set.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/22 下午9:53
# -------------------------------------------------------------------------------
from rest_framework import serializers

from apps.BasicAuthService.serializers import BaseListSerializer, BaseSerializer
from apps.HttpAutoTestService.models import TestsuiteSet
from apps.HttpAutoTestService.serializers import TidyProjectSerializer

__all__ = ['TestsuiteSetListSerializer', 'TestsuiteSetSerializer']


class TestsuiteSetListSerializer(BaseListSerializer):
    project = TidyProjectSerializer()
    testsuites = serializers.SerializerMethodField()

    def get_testsuites(self, obj):
        result = obj.testsuites.order_by('testsuite_set2testsuite').values('id', 'name', 'is_deleted')
        return result

    class Meta:
        model = TestsuiteSet
        fields = '__all__'


class TestsuiteSetSerializer(BaseSerializer):
    class Meta:
        model = TestsuiteSet
        exclude = ('status',)
