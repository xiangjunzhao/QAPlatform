# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/20 9:05
# -------------------------------------------------------------------------------
from django.db import models

from apps.HttpAutoTestService.models import HttpBaseModel

__all__ = ['Testsuite']


class Testsuite(HttpBaseModel):
    level_choice = (
        ('LOW', '低'),
        ('NORMAL', '中'),
        ('HIGH', '高'),
        ('HIGHER', '更高'),
    )
    level = models.CharField(max_length=12, null=False, blank=False, default='NORMAL', choices=level_choice,
                             verbose_name='场景级别')
    status_choice = (
        ('INITIAL', '初始状态'),
        ('FAIL', '全部失败'),
        ('PASS', '全部通过'),
        ('PARTIAL_PASS', '部分通过')
    )
    status = models.CharField(max_length=12, null=False, blank=False, default='INITIAL', choices=status_choice,
                              verbose_name='是否测试通过')
    global_variables = models.JSONField(null=True, blank=True, verbose_name='场景全局参数')
    project = models.ForeignKey(to='Project', related_name='testsuite', on_delete=models.SET_NULL, null=True,
                                blank=True, verbose_name='所属项目')
    enable_private_env = models.BooleanField(null=False, blank=False, default=False, verbose_name='开启/关闭私有环境')
    private_environment = models.ForeignKey(to='Environment', related_name='+', on_delete=models.SET_NULL, null=True,
                                            blank=True, verbose_name='私有测试环境')

    class Meta:
        verbose_name = '测试场景'
        verbose_name_plural = verbose_name
        db_table = 'http_testsuite'
        ordering = ('-update_time',)

    def __str__(self):
        return self.name
