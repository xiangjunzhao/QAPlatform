# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         random_str.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/4/28 11:32
# -------------------------------------------------------------------------------
import uuid
from django.db import models

__all__ = ['RandomStr']


class RandomStr(models.Model):
    id = models.CharField(primary_key=True, max_length=36, default=uuid.uuid4)
    random_str = models.CharField(null=False, blank=False, max_length=36, verbose_name='随机字符串')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')

    class Meta:
        verbose_name = '随机字符串'
        verbose_name_plural = verbose_name
        db_table = 'http_random_str'
        ordering = ('random_str',)

    def __str__(self):
        return self.random_str
