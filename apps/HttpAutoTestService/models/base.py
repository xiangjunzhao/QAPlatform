# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         base
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-17
# -------------------------------------------------------------------------------
import uuid
from django.db import models

from apps.BasicAuthService.models import User
from apps.BasicAuthService.models.base import BaseModel
from utils.common import get_version as version

__all__ = ['HttpBaseModel', 'HttpBaseResultModel']


class HttpBaseModel(BaseModel):
    name = models.CharField(null=False, blank=False, max_length=128, verbose_name='名称')
    creator = models.ForeignKey(to=User, related_name='+', on_delete=models.SET_NULL, null=True, blank=True,
                                verbose_name='创建人')
    modifier = models.ForeignKey(to=User, related_name='+', on_delete=models.SET_NULL, null=True, blank=True,
                                 verbose_name='修改人')

    class Meta:
        abstract = True


class HttpBaseResultModel(models.Model):
    id = models.CharField(primary_key=True, max_length=36, default=uuid.uuid4)
    execute_time = models.DateTimeField(verbose_name='执行时间')
    elapsed_ms = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, verbose_name='执行时长(ms)')
    status_choice = (
        ('PASS', '通过'),
        ('FAIL', '失败')
    )
    status = models.CharField(max_length=11, null=True, blank=True, choices=status_choice, verbose_name='是否测试通过')
    failure_reason = models.JSONField(null=True, blank=True, verbose_name='测试未通过原因')
    executor = models.ForeignKey(to=User, related_name='+', on_delete=models.SET_NULL, null=True, blank=True,
                                 verbose_name='执行人')
    executor_real_name = models.CharField(null=False, blank=False, max_length=32, default='定时任务',
                                          verbose_name='执行人真实姓名')
    is_periodictask = models.BooleanField(null=False, blank=False, default=True, verbose_name='是否是定时任务')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    is_deleted = models.BooleanField(null=False, blank=False, default=False, verbose_name='是否已删除')
    version = models.CharField(max_length=13, null=False, blank=False, db_index=True, default=version,
                               verbose_name='版本号')
    output_result = models.JSONField(null=True, blank=True, verbose_name='输出变量结果')
    context_global_variable = models.JSONField(null=True, blank=True, verbose_name='上下文全局变量')

    class Meta:
        abstract = True
