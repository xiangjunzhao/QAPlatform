# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite_set.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/22 下午9:36
# -------------------------------------------------------------------------------
from django.db import models

from apps.HttpAutoTestService.models import HttpBaseModel

__all__ = ['TestsuiteSet']


class TestsuiteSet(HttpBaseModel):
    level_choice = (
        ('LOW', '低'),
        ('NORMAL', '中'),
        ('HIGH', '高'),
        ('HIGHER', '更高'),
    )
    level = models.CharField(max_length=12, null=False, blank=False, default='NORMAL', choices=level_choice,
                             verbose_name='场景级别')
    status_choice = (
        ('INITIAL', '初始状态'),
        ('FAIL', '全部失败'),
        ('PASS', '全部通过'),
        ('PARTIAL_PASS', '部分通过')
    )
    status = models.CharField(max_length=12, null=False, blank=False, default='INITIAL', choices=status_choice,
                              verbose_name='是否测试通过')
    project = models.ForeignKey(to='Project', related_name='testsuite_set', on_delete=models.SET_NULL, null=True,
                                blank=True, verbose_name='所属项目')
    testsuites = models.ManyToManyField(to='Testsuite', related_name='testsuite_set',
                                        through_fields=('testsuite_set', 'testsuite'), through='TestsuiteSet2Testsuite',
                                        verbose_name='测试场景')

    class Meta:
        verbose_name = '测试场景集'
        verbose_name_plural = verbose_name
        db_table = 'http_testsuite_set'
        ordering = ('-update_time',)

    def __str__(self):
        return self.name
