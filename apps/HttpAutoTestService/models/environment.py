# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         environment.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-17
# -------------------------------------------------------------------------------
from django.db import models

from apps.HttpAutoTestService.models import HttpBaseModel

__all__ = ['Environment']


class Environment(HttpBaseModel):
    name = models.CharField(null=False, blank=False, max_length=128, verbose_name='名称')
    base_url = models.CharField(null=False, blank=False, max_length=256, verbose_name='环境地址')
    global_variables = models.JSONField(null=True, blank=True, verbose_name='测试环境全局参数')

    class Meta:
        verbose_name = '测试环境'
        verbose_name_plural = verbose_name
        db_table = 'http_environment'
        ordering = ('name',)

    def __str__(self):
        return self.name
