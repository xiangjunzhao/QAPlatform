# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         module.py
# Description:  扩展方法模型
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 16:49
# -------------------------------------------------------------------------------
from django.db import models

from apps.HttpAutoTestService.models import HttpBaseModel

__all__ = ['Module']


class Module(HttpBaseModel):
    project = models.ForeignKey(to='Project', related_name='module', on_delete=models.SET_NULL, null=True, blank=True,
                                verbose_name='所属项目')
    parent = models.ForeignKey(to='Module', related_name='child', on_delete=models.SET_NULL, null=True, blank=True,
                               verbose_name='父节点')
    is_system = models.BooleanField(null=False, blank=False, default=False, verbose_name='是否系统内置')

    class Meta:
        verbose_name = '模块'
        verbose_name_plural = verbose_name
        db_table = 'http_module'
        ordering = ('name',)

    def __str__(self):
        return self.name
