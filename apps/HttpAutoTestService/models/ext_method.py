# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         ext_method.py
# Description:  扩展方法
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/13 下午5:11
# -------------------------------------------------------------------------------
from django.db import models

from apps.HttpAutoTestService.models import HttpBaseModel

__all__ = ['ExtMethod']


class ExtMethod(HttpBaseModel):
    ext_method = models.CharField(null=False, blank=False, max_length=256, verbose_name='扩展方法')
    ext_method_desc = models.CharField(null=True, blank=True, max_length=256, verbose_name='扩展方法说明')
    args_desc = models.CharField(null=True, blank=True, max_length=256, verbose_name='参数说明')
    returned_value_desc = models.CharField(null=True, blank=True, max_length=256, verbose_name='返回值说明')
    type_choice = (
        ('BUILT_IN_KEYWORD', '内置关键字'),
        ('BUSINESS_KEYWORD', '业务关键字')
    )
    type = models.CharField(max_length=16, null=False, blank=False, default='BUILT_IN_KEYWORD', choices=type_choice,
                            verbose_name='扩展方法类型')

    class Meta:
        verbose_name = '扩展方法'
        verbose_name_plural = verbose_name
        db_table = 'http_ext_method'
        ordering = ('ext_method',)

    def __str__(self):
        return self.name
