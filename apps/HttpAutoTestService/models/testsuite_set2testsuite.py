# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testsuite_set2testsuite.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/22 下午9:43
# -------------------------------------------------------------------------------

from django.db import models

__all__ = ['TestsuiteSet2Testsuite']


class TestsuiteSet2Testsuite(models.Model):
    testsuite_set = models.ForeignKey(to='TestsuiteSet', related_name='testsuite_set2testsuite',
                                      on_delete=models.CASCADE, verbose_name='测试场景集')
    testsuite = models.ForeignKey(to='Testsuite', related_name='testsuite_set2testsuite', on_delete=models.CASCADE,
                                  verbose_name='测试场景', null=True)
    sort = models.IntegerField(null=False, blank=False, verbose_name='排序')
    is_execute = models.BooleanField(null=False, blank=False, default=False, verbose_name='执行/跳过')
    loop_count = models.IntegerField(null=False, blank=False, default=1, verbose_name='循环次数')

    # 冗余场景集字段：testsuite_set_name
    testsuite_set_name = models.CharField(null=False, blank=False, max_length=128, verbose_name='测试场景集名称')
    # 冗余场景字段：testsuite_name
    testsuite_name = models.CharField(null=True, blank=True, max_length=128, verbose_name='测试场景名称')

    class Meta:
        verbose_name = '场景集场景'
        verbose_name_plural = verbose_name
        db_table = 'http_testsuite_set_testsuites'
        ordering = ('sort',)
