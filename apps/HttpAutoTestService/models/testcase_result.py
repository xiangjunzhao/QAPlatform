# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testcase_result.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/21 9:13
# -------------------------------------------------------------------------------
from django.db import models
from apps.HttpAutoTestService.models import HttpBaseResultModel

__all__ = ['TestcaseResult']


class TestcaseResult(HttpBaseResultModel):
    url = models.TextField(null=False, blank=False, verbose_name='接口请求URL')
    method_choice = (
        ('GET', 'GET'),
        ('PUT', 'PUT'),
        ('POST', 'POST'),
        ('PATCH', 'PATCH'),
        ('DELETE', 'DELETE')
    )
    method = models.CharField(max_length=11, null=False, blank=False, default='GET', choices=method_choice,
                              verbose_name='接口请求方法')
    headers = models.JSONField(null=True, blank=True, verbose_name='接口请求头信息')
    request_data_type_choice = (
        ('Params', 'Params'),
        ('Json', 'Json'),
        ('Form Data', 'Form Data')
    )
    request_data_type = models.CharField(max_length=11, null=False, blank=False, default='Json',
                                         choices=request_data_type_choice, verbose_name='接口请求参数类型')
    request_data = models.JSONField(null=True, blank=True, verbose_name='接口请求参数')
    actual_status_code = models.CharField(max_length=11, null=False, blank=False, verbose_name='实际响应状态码')
    actual_response_data = models.JSONField(null=False, blank=False, verbose_name='实际响应结果')
    elapsed = models.DecimalField(max_digits=10, decimal_places=3, null=False, blank=False, verbose_name='响应时长(ms)')
    project = models.ForeignKey(to='Project', related_name='+', on_delete=models.SET_NULL, null=True, blank=True,
                                verbose_name='所属项目')
    project_name = models.CharField(null=False, blank=False, max_length=128, verbose_name='项目名称')
    api = models.ForeignKey(to='Api', related_name='+', on_delete=models.SET_NULL, null=True, blank=True,
                            verbose_name='所属接口')
    api_name = models.CharField(null=False, blank=False, max_length=128, verbose_name='接口名称')
    testcase = models.ForeignKey(to='Testcase', related_name='testcase_result', on_delete=models.SET_NULL, null=True,
                                 blank=True, verbose_name='所属用例')
    testcase_name = models.CharField(null=False, blank=False, max_length=128, verbose_name='用例名称')
    testsuite_name = models.CharField(null=True, blank=True, max_length=128, verbose_name='场景名称')
    testsuite_result = models.ForeignKey(to='TestsuiteResult', related_name='testcase_result',
                                         on_delete=models.CASCADE, null=True, blank=True, verbose_name='所属场景结果')

    class Meta:
        verbose_name = '用例结果'
        verbose_name_plural = verbose_name
        db_table = 'http_testcase_result'
        ordering = ('-execute_time',)

    def __str__(self):
        return self.testcase_name
