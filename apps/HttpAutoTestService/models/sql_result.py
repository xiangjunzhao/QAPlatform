# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         sql_result.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2022/8/15 10:44
# -------------------------------------------------------------------------------
from django.db import models
from apps.HttpAutoTestService.models import HttpBaseResultModel

__all__ = ['SQLResult']


class SQLResult(HttpBaseResultModel):
    sql = models.TextField(null=True, blank=True, verbose_name='数据库SQL')
    sql_desc = models.TextField(null=True, blank=True, verbose_name='数据库SQL功能描述')
    sql_ext = models.JSONField(null=True, blank=True, verbose_name='数据库SQL信息')
    sql_run_result = models.JSONField(null=True, blank=True, verbose_name='数据库SQL执行结果')
    testsuite_name = models.CharField(null=False, blank=False, max_length=128, verbose_name='场景名称')
    testsuite_result = models.ForeignKey(to='TestsuiteResult', related_name='sql_result',
                                         on_delete=models.CASCADE, null=False, blank=False, verbose_name='所属场景结果')

    class Meta:
        verbose_name = '数据库SQL执行结果'
        verbose_name_plural = verbose_name
        db_table = 'http_sql_result'
        ordering = ('-execute_time',)

    def __str__(self):
        return self.sql_desc
