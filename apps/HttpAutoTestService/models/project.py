# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         project.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 9:38
# -------------------------------------------------------------------------------
from django.db import models

from apps.BasicAuthService.models import User
from apps.HttpAutoTestService.models import HttpBaseModel

__all__ = ['Project']


class Project(HttpBaseModel):
    name = models.CharField(null=False, blank=False, max_length=128, unique=True, verbose_name='名称')
    global_variables = models.JSONField(null=True, blank=True, verbose_name='项目全局参数')
    default_environment = models.ForeignKey(to='Environment', related_name='+', on_delete=models.SET_NULL, null=True,
                                            blank=True, verbose_name='默认测试环境')
    environments = models.ManyToManyField(to='Environment', related_name='project', verbose_name='环境')
    members = models.ManyToManyField(User, related_name='project', verbose_name='项目成员')

    class Meta:
        verbose_name = '项目'
        verbose_name_plural = verbose_name
        db_table = 'http_project'
        ordering = ('name',)

    def __str__(self):
        return self.name
