# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         ext_method_result.py
# Description:  扩展方法结果模型
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/14 下午10:46
# -------------------------------------------------------------------------------
from django.db import models
from apps.HttpAutoTestService.models import HttpBaseResultModel

__all__ = ['ExtMethodResult']


class ExtMethodResult(HttpBaseResultModel):
    ext_method_name = models.CharField(null=True, blank=True, max_length=128, verbose_name='扩展方法名称')
    ext_method = models.TextField(null=False, blank=False, verbose_name='扩展方法')
    ext_method_run_result = models.JSONField(null=True, blank=True, verbose_name='扩展方法运行结果')
    testsuite_name = models.CharField(null=False, blank=False, max_length=128, verbose_name='场景名称')
    testsuite_result = models.ForeignKey(to='TestsuiteResult', related_name='ext_method_result',
                                         on_delete=models.CASCADE, null=False, blank=False, verbose_name='所属场景结果')

    class Meta:
        verbose_name = '扩展方法运行结果'
        verbose_name_plural = verbose_name
        db_table = 'http_ext_method_result'
        ordering = ('-execute_time',)

    def __str__(self):
        return self.ext_method_name or self.ext_method
