# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         test_trend_result.py
# Description:  测试趋势结果
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/1/29 9:11
# -------------------------------------------------------------------------------
import uuid

from django.db import models

__all__ = ['TestTrendResult']


class TestTrendResult(models.Model):
    id = models.CharField(primary_key=True, max_length=36, default=uuid.uuid4)
    total_api_count = models.IntegerField(null=False, blank=False, default=0, verbose_name='项目接口总数')
    covered_api_count = models.IntegerField(null=False, blank=False, default=0, verbose_name='覆盖接口总数')
    pass_testcase_count = models.IntegerField(null=False, blank=False, default=0, verbose_name='通过用例总数')
    fail_testcase_count = models.IntegerField(null=False, blank=False, default=0, verbose_name='失败用例总数')
    total_testcase_count = models.IntegerField(null=False, blank=False, default=0, verbose_name='执行用例总数')
    project = models.ForeignKey(to='Project', related_name='+', on_delete=models.SET_NULL, null=True, blank=True,
                                verbose_name='所属项目')
    execute_date = models.DateField(auto_now_add=False, verbose_name='执行日期')

    class Meta:
        verbose_name = '测试趋势结果'
        verbose_name_plural = verbose_name
        db_table = 'http_test_trend_result'
        ordering = ('execute_date',)
