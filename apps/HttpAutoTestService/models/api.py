# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         api.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:32
# -------------------------------------------------------------------------------
from django.db import models
from apps.HttpAutoTestService.models import HttpBaseModel

__all__ = ['Api']


class Api(HttpBaseModel):
    url = models.TextField(null=False, blank=False, verbose_name='接口请求URL')
    method_choice = (
        ('GET', 'GET'),
        ('PUT', 'PUT'),
        ('POST', 'POST'),
        ('PATCH', 'PATCH'),
        ('DELETE', 'DELETE'),
        ('OPTIONS', 'OPTIONS'),
        ('HEAD', 'HEAD'),
    )
    method = models.CharField(max_length=11, null=False, blank=False, default='GET', choices=method_choice,
                              verbose_name='接口请求方法')
    headers = models.JSONField(null=True, blank=True, verbose_name='接口请求头信息')
    request_data_type_choice = (
        ('Json', 'Json'),
        ('Form Data', 'Form Data')
    )
    request_data_type = models.CharField(max_length=11, null=False, blank=False, default='Json',
                                         choices=request_data_type_choice, verbose_name='接口请求参数类型')
    request_params = models.TextField(null=True, blank=True, verbose_name='接口查询参数')
    request_data = models.JSONField(null=True, blank=True, verbose_name='接口请求体参数')
    parameter_desc = models.TextField(null=True, blank=True, default="", verbose_name='接口参数定义')
    project = models.ForeignKey(to='Project', related_name='api', on_delete=models.SET_NULL, null=True, blank=True,
                                verbose_name='所属项目')
    module = models.ForeignKey(to='Module', related_name='api', on_delete=models.SET_NULL, null=True, blank=True,
                               verbose_name='所属模块')
    environment = models.ForeignKey(to='Environment', related_name='environment', on_delete=models.SET_NULL, null=True,
                                    blank=True, verbose_name='运行环境')

    class Meta:
        verbose_name = '接口'
        verbose_name_plural = verbose_name
        db_table = 'http_api'
        ordering = ('-update_time',)

    def __str__(self):
        return self.name
