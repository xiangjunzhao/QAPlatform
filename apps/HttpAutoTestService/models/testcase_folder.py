# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testcase_folder.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/27 下午8:14
# -------------------------------------------------------------------------------
from django.db import models

from apps.HttpAutoTestService.models import HttpBaseModel

__all__ = ['TestcaseFolder']


class TestcaseFolder(HttpBaseModel):
    project = models.ForeignKey(to='Project', related_name='testcase_folder', on_delete=models.SET_NULL, null=True,
                                blank=True, verbose_name='所属项目')
    parent = models.ForeignKey(to='TestcaseFolder', related_name='+', on_delete=models.SET_NULL, null=True, blank=True,
                               verbose_name='父节点')
    is_system = models.BooleanField(null=False, blank=False, default=False, verbose_name='是否系统内置')

    class Meta:
        verbose_name = '用例夹'
        verbose_name_plural = verbose_name
        unique_together = ['name', 'project']
        db_table = 'http_testcase_folder'
        ordering = ('name',)

    def __str__(self):
        return self.name
