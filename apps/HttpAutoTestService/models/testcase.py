# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         testcase.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-19
# -------------------------------------------------------------------------------
from django.db import models

from apps.HttpAutoTestService.models import HttpBaseModel

__all__ = ['Testcase']


class Testcase(HttpBaseModel):
    name = models.CharField(null=False, blank=False, max_length=128, unique=True, verbose_name='名称')
    url = models.TextField(null=False, blank=False, verbose_name='接口请求URL')
    headers = models.JSONField(null=True, blank=True, verbose_name='接口请求头')
    request_data_type_choice = (
        ('Json', 'Json'),
        ('Form Data', 'Form Data')
    )
    request_data_type = models.CharField(max_length=11, null=False, blank=False, default='Json',
                                         choices=request_data_type_choice, verbose_name='接口请求参数类型')
    request_params = models.TextField(null=True, blank=True, verbose_name='接口查询参数')
    request_data = models.JSONField(null=True, blank=True, verbose_name='接口请求参数')
    expect_result = models.JSONField(null=True, blank=True, verbose_name='期望结果')
    level_choice = (
        ('LOW', '低'),
        ('NORMAL', '中'),
        ('HIGH', '高'),
        ('HIGHER', '更高'),
    )
    level = models.CharField(max_length=12, null=False, blank=False, default='NORMAL', choices=level_choice,
                             verbose_name='用例级别')
    status_choice = (
        ('INITIAL', '初始状态'),
        ('PASS', '通过'),
        ('FAIL', '失败')
    )
    status = models.CharField(max_length=11, null=False, blank=False, default='INITIAL', choices=status_choice,
                              verbose_name='是否测试通过')
    api = models.ForeignKey(to='Api', related_name='testcase', on_delete=models.SET_NULL, null=True, blank=True,
                            verbose_name='所属接口')
    testcase_folder = models.ForeignKey(to='TestcaseFolder', related_name='testcase_folder', on_delete=models.SET_NULL,
                                        null=True, blank=True, verbose_name='所属用例夹')

    class Meta:
        verbose_name = '测试用例'
        verbose_name_plural = verbose_name
        db_table = 'http_testcase'
        ordering = ('-update_time',)

    def __str__(self):
        return self.name
