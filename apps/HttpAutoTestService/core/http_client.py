# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         http_client.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:32
# -------------------------------------------------------------------------------

import logging
from json import JSONDecodeError
import requests
from django.utils import timezone
from requests import Response, Request
from requests.exceptions import MissingSchema, InvalidSchema, InvalidURL, RequestException

logger = logging.getLogger(__name__)


class HttpSession(requests.Session):
    """
    执行HTTP请求，并在请求之间保存session、cookies。
    """

    def __init__(self, *args, **kwargs):
        super(HttpSession, self).__init__(*args, **kwargs)
        self.__init_response_meta()

    def __init_response_meta(self):
        """
        初始化meta数据，用于存储响应的详细数据
        Returns:

        """

        self.response_meta_data = {
            "response": {
                "url": "",
                "status_code": 200,
                "elapsed": 0,
                "cookies": {},
                "headers": {},
                "results": None,
                "content": None,
                "text": None,
            },
            "stat": {
                "execute_time": None,
            }
        }

    @staticmethod
    def _get_response_records(resp_obj):
        """
        从Response()对象中获取响应的信息
        Args:
            resp_obj: 响应对象

        Returns: 处理后的响应dict

        """

        # 响应字典
        response_dict = dict()

        # 获取响应信息
        response_dict["url"] = resp_obj.url
        response_dict["status_code"] = resp_obj.status_code
        # 响应时长（ms）
        response_dict["elapsed"] = resp_obj.elapsed.microseconds / 1000.0
        response_dict["cookies"] = resp_obj.cookies or {}
        response_dict["headers"] = dict(resp_obj.headers)

        content_type = dict(resp_obj.headers).get("Content-Type", '')
        try:
            # 记录响应的json数据
            response_dict["results"] = resp_obj.json()
        except JSONDecodeError:
            if "image" in content_type:
                response_dict["content"] = resp_obj.content
            else:
                text = resp_obj.text
                response_dict["text"] = text
        return response_dict

    def request(self, method, url, **kwargs):
        """
        构造请并发送请求
        Args:
            method:
            url:
            **kwargs:

        Returns:

        """

        self.__init_response_meta()
        # 记录测试名称
        kwargs.setdefault("timeout", 120)

        # 执行时间
        execute_time = timezone.now()
        self.response_meta_data["stat"].update({"execute_time": execute_time})

        # 发送请求，并返回响应内容
        response = self._send_request_safe_mode(method, url, **kwargs)

        # 记录请求和响应，不包括30X重定向
        self.response_meta_data["response"] = self._get_response_records(response)

        try:
            response.raise_for_status()
        except RequestException as e:
            logger.error("exception：{}".format(str(e)))

        return self.response_meta_data

    def _send_request_safe_mode(self, method, url, **kwargs):
        """
        发送一个HTTP请求，并捕获由于连接问题可能发生的任何异常。
        Args:
            method:
            url:
            **kwargs:

        Returns:

        """

        try:
            return requests.Session.request(self, method=method, url=url, **kwargs)
        except (MissingSchema, InvalidSchema, InvalidURL):
            raise
        except RequestException as e:
            r = HttpResponse()
            r.error = e
            r.status_code = 0  # with this status_code, content returns None
            r.request = Request(method, url).prepare()
            return r


class HttpResponse(Response):
    """
    HTTP请求响应对象
    """

    def raise_for_status(self):
        if hasattr(self, "error") and self.error:
            raise self.error
        Response.raise_for_status(self)
