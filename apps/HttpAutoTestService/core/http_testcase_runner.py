# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         http_testcase_runner.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:32
# -------------------------------------------------------------------------------
import json
import logging
import time

import retrying
from django.utils import timezone

from apps.HttpAutoTestService.core import utils
from apps.HttpAutoTestService.core.http_client import HttpSession
from apps.HttpAutoTestService.core.http_session_context import HttpSessionContext
from apps.HttpAutoTestService.core.utils import regex_parse_args, parse_output_result, \
    parse_http_testcase_validate_result
from apps.HttpAutoTestService.models import TestcaseResult, Environment, Project, Api, Testcase

logger = logging.getLogger(__name__)


class HttpTestcaseRunner(object):

    def __init__(self, http_session: HttpSession = None, http_session_context: HttpSessionContext = None,
                 environment: Environment = None, project: Project = None, api: Api = None, testcase: Testcase = None):
        """
        实例化用例运行器
        Args:
            http_session:
            http_session_context:
            environment: 运行环境
            project: 项目
            api: 接口
            testcase: 测试用例
        """
        self.http_session = http_session
        self.http_session_context = http_session_context
        self.environment = environment
        self.project = project
        self.api = api
        self.testcase = testcase

    @retrying.retry(stop_max_attempt_number=3)
    def run(self, testcase_name=None, url=None, headers=None, request_params=None, request_data_type=None,
            request_data=None, expect_result=None, executor=None, is_periodictask=True) -> TestcaseResult:
        """
        运行测试用命
        Args:
            testcase_name: 用例名称
            url: 请求url
            headers: 请求头
            request_params: 查询参数
            request_data_type: 请求参数类型
            request_data: 请求参数
            expect_result: 期望结果
            executor: 执行者
            is_periodictask: 是否是定时任务

        Returns:

        """
        # 执行时间
        execute_time = timezone.now()
        # 开始时间，毫秒
        start_ms = time.time() * 1000
        if self.testcase:
            # 执行的是批量选中的测试用例
            testcase_name = self.testcase.name
            url = self.testcase.url
            headers = self.testcase.headers
            request_params = self.testcase.request_params
            request_data_type = self.testcase.request_data_type
            request_data = self.testcase.request_data
            expect_result = self.testcase.expect_result

        logger.info(f'=============== 执行测试用例:【{testcase_name}】===============')
        method = self.api.method.upper()
        request_kwargs = dict()

        testcase_result_mapping = {
            "project_id": self.project.id,
            "project_name": self.project.name,
            "api_id": self.api.id,
            "api_name": self.api.name,
            "testcase_id": self.testcase.id if self.testcase else None,
            "testcase_name": testcase_name,
            "url": url,
            "method": method,
            "headers": headers,
            "request_data_type": request_data_type,
            "request_data": request_data,
        }
        if not is_periodictask:
            testcase_result_mapping.update({
                'executor_id': executor.id,
                'executor_real_name': executor.real_name,
                'is_periodictask': is_periodictask
            })

        if not (self.environment.is_deleted or self.project.is_deleted or self.api.is_deleted):
            variables_mapping = {**self.http_session_context.session_variables_mapping,
                                 **self.http_session_context.output_variables_mapping}
            logger.info('测试用例【{testcase_name}】执行前，全局参数内容：{variables_mapping}'.format(testcase_name=testcase_name,
                                                                                     variables_mapping=variables_mapping))

            # 解析接口测试用例参数
            request_arg = None  # 标记正在解析的参数
            try:
                request_arg = "请求URL"
                base_url = self.environment.base_url
                url = utils.build_url(base_url=base_url, url=url)
                url = regex_parse_args(content=url, http_session_context=self.http_session_context)
                if request_params:
                    request_arg = "请求URL"
                    request_params = regex_parse_args(content=request_params,
                                                      http_session_context=self.http_session_context)
                    logger.info('请求查询参数：{request_params}'.format(request_params=request_params))
                    if '?' in url:
                        url = "{url}&{params}".format(url=url.rstrip('&'), params=request_params.lstrip('&'))
                    else:
                        url = "{url}?{params}".format(url=url.rstrip('/'), params=request_params.lstrip('?'))
                logger.info('请求URL：{url}'.format(url=url))

                logger.info('请求方法：{method}'.format(method=method))

                request_arg = "Headers参数"
                headers = regex_parse_args(content=headers, http_session_context=self.http_session_context)
                headers = json.loads(headers) if headers else {}
                request_kwargs.update({"headers": headers})
                logger.info('请求头信息：{headers}'.format(headers=headers))

                logger.info('请求体参数类型：{request_data_type}'.format(request_data_type=request_data_type))

                request_arg = "Body参数"
                request_data = regex_parse_args(content=request_data, http_session_context=self.http_session_context)
                request_data = json.loads(request_data) if request_data else {}
                logger.info('请求体参数：{request_data}'.format(request_data=request_data))

                if request_data_type == "Json":
                    request_kwargs.update({"json": request_data})
                elif request_data_type == "Form Data":
                    request_kwargs.update({"data": request_data})

                # 期望结果
                request_arg = "期望结果"
                expect_result = regex_parse_args(content=expect_result, http_session_context=self.http_session_context)
                expect_result = json.loads(expect_result) if expect_result else {}
                logger.info('期望结果：{expect_result}'.format(expect_result=expect_result))
            except Exception as e:
                testcase_result_mapping.update({
                    "actual_status_code": 0,
                    "actual_response_data": "",
                    "execute_time": timezone.now(),
                    "elapsed_ms": 0,
                    "elapsed": 0,
                    "status": "FAIL",
                    "failure_reason": f"解析接口测试用例参数【{request_arg}】异常，异常原因：{repr(e)}",
                    "context_global_variable": variables_mapping
                })
                testcase_result = TestcaseResult(**testcase_result_mapping)
                return testcase_result

            resp_obj = self.http_session.request(method=method, url=url, **request_kwargs)
            # 获取HttpSession对象request方法返回数据的keys
            response_keys = resp_obj["response"].keys()
            # 获取请求返回数据实际存储的key
            actual_response_key = list(set(response_keys).intersection(['results', 'text', 'content']))[0]
            actual_response_data = resp_obj["response"][actual_response_key]
            logger.info(f'响应结果：{actual_response_data}')

            if resp_obj["response"]["cookies"]:
                # 获取响应cookies
                cookies = resp_obj["response"]["cookies"].get_dict()
                # 将cookies更新到session中
                self.http_session.cookies.update(cookies)

            # 存储提取输出的变量值
            output_result = parse_output_result(name=testcase_name, result=resp_obj["response"],
                                                expect_result=expect_result,
                                                http_session_context=self.http_session_context)
            # 校验测试用例执行结果
            validate_pass, failure_reason = parse_http_testcase_validate_result(
                result=resp_obj["response"], expect_result=expect_result,
                http_session_context=self.http_session_context)
            # 结束时间，毫秒
            end_ms = time.time() * 1000
            # 执行耗时，毫秒
            elapsed_ms = round(end_ms - start_ms, 3)
            testcase_result_mapping.update({
                "url": url,
                "headers": headers,
                "request_data_type": request_data_type,
                "request_data": request_data,
                "actual_status_code": resp_obj["response"]["status_code"],
                "actual_response_data": actual_response_data,
                "execute_time": execute_time,
                "elapsed_ms": elapsed_ms,
                "elapsed": resp_obj["response"]["elapsed"],
                "status": validate_pass,
                "output_result": output_result if output_result else "",
                "failure_reason": failure_reason if validate_pass == "FAIL" else "",
                "context_global_variable": variables_mapping
            })

            if validate_pass == 'FAIL':
                logger.warning('测试用例：{name}，测试未通过；原因：{failure_reason}'.format(name=testcase_name,
                                                                              failure_reason=failure_reason))
            testcase_result = TestcaseResult(**testcase_result_mapping)
            return testcase_result
