# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         ext_time.py
# Description:  time相关扩展方法
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/12/12 下午7:49
# -------------------------------------------------------------------------------

import logging
import time

__all__ = ['sleep']

logger = logging.getLogger(__name__)


def sleep(seconds):
    logger.info('正在执行sleep({seconds})方法，程序将暂停{seconds}秒'.format(seconds=seconds))
    time.sleep(seconds)
