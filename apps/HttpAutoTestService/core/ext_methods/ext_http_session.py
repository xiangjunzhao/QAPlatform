# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         ext_http_session.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2021/11/26 10:34 上午
# -------------------------------------------------------------------------------
from apps.HttpAutoTestService.core.http_client import HttpSession
from apps.HttpAutoTestService.core.http_session_context import HttpSessionContext

__all__ = ['add_or_update_cookies', 'clear_cookie', 'clear_cookies', 'add_or_update_output_variables',
           'clear_output_variable', 'clear_output_variables']


def add_or_update_cookies(http_session: HttpSession, cookies: dict):
    """
    添加、更新cookie信息
    Args:
        http_session:
        cookies:cookie信息，字典类型

    Returns:

    """
    if isinstance(cookies, dict) and cookies:
        http_session.cookies.update(cookies)


def clear_cookie(http_session: HttpSession, key: str):
    """
    清除指定的cookie
    Args:
        http_session:
        key:cookie键名

    Returns:

    """
    http_session.cookies.pop(key=key)


def clear_cookies(http_session: HttpSession):
    """
    清除所有cookies
    Args:
        http_session:

    Returns:

    """
    http_session.cookies.clear_session_cookies()


def add_or_update_output_variables(http_session_context: HttpSessionContext, variables: dict):
    """
    添加、更新输出变量
    Args:
        http_session_context:
        variables:

    Returns:

    """
    http_session_context.update_output_variables(variables)


def clear_output_variable(http_session_context: HttpSessionContext, key: str):
    """
    清除指定的输出变量
    Args:
        http_session_context:
        key:

    Returns:

    """
    if key in http_session_context.output_variables_mapping.keys():
        http_session_context.output_variables_mapping.pop(key)


def clear_output_variables(http_session_context: HttpSessionContext):
    """
    清除所有的输出变量
    Args:
        http_session_context:

    Returns:

    """
    http_session_context.output_variables_mapping = {}


if __name__ == '__main__':
    http_session = HttpSession()
    add_or_update_cookies(http_session, {'PHPSESSIONID': '1234567'})
    print(http_session.cookies.get_dict())
    clear_cookie(http_session, 'PHPSESSIONID')
    print(http_session.cookies.get_dict())
    add_or_update_cookies(http_session, {'PHPSESSIONID': '1234567'})
    print(http_session.cookies.get_dict())
    clear_cookies(http_session)
    print(http_session.cookies.get_dict())

    http_session_context = HttpSessionContext()
    http_session_context.update_output_variables({'name': 'zxj', 'age': 18})
    clear_output_variable(http_session_context, 'name')
    print(http_session_context.output_variables_mapping)
    clear_output_variable(http_session_context, 'name1')
