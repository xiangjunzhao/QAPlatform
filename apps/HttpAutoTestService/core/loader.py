# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         loader.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:32
# -------------------------------------------------------------------------------
import types

from apps.HttpAutoTestService.core.builtin import functions, comparators


def load_built_in_functions():
    """
    加载builtin包中的内建方法
    Returns:

    """
    built_in_functions = {}
    for name, item in vars(functions).items():
        if isinstance(item, types.FunctionType):
            built_in_functions[name] = item

    return built_in_functions


def load_built_in_comparators():
    """
    加载builtin包中的内建比较器
    Returns:

    """
    built_in_comparators = {}
    for name, item in vars(comparators).items():
        if isinstance(item, types.FunctionType):
            built_in_comparators[name] = item

    return built_in_comparators
