# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         parser.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/18 17:32
# -------------------------------------------------------------------------------
import json
import logging
import re

from apps.HttpAutoTestService.core.builtin.functions import *
from apps.HttpAutoTestService.core.extractor import Extractor

logger = logging.getLogger(__name__)

# 变量正则表达式1，示例：{{var}},{{var[1]}},{{var[1:5:2]}}
variable_regex_compile1 = re.compile(r"\{\{(\w+)(\[([-:\d]+)\])?\}\}")
# 变量正则表达式2，示例：${var},${var[1]},${var[1:5:2]}
variable_regex_compile2 = re.compile(r"\$\{(\w+)(\[([-:\d]+)\])?\}")
# 方法正则表达式，示例：$func1({{var_1}}, {{var_3}})$、$func1(${var_1}, ${var_3})$
function_regex_compile = re.compile(r"\$(\w+\(.*?\))\$")


def regex_replace_variables(content, http_session_context):
    """
    查询content中符合variable_regex_compile正则表达式的变量字符串，
    再从http_session_context中提取相应变量值，
    再将变量值替换掉content中的变量字符串
    Args:
        content:
        http_session_context: HttpSessionContext实例

    Returns:

    """

    def parse_variables(variable_regex_compile, content, http_session_context):
        """
        根据正则表达式，将content中符合正则表达式的内容用variables_mapping中的内容进行替换
        Args:
            variable_regex_compile:变量正则表达式
            content:
            http_session_context: HttpSessionContext实例

        Returns:

        """
        content = content if isinstance(content, str) else json.dumps(content, ensure_ascii=False)
        while variable_regex_compile.search(content):
            replaced_key = temp_replaced_key = variable_regex_compile.search(content).group(0)
            logger.info(f'正在解析变量：{temp_replaced_key}')
            variable_key = variable_regex_compile.search(content).group(1)
            # 列表切片
            slice_key = variable_regex_compile.search(content).group(3)
            replaced_value = http_session_context.get_variable(variable_key)
            if slice_key:
                # 执行列表切片功能
                slice_value = slice_key.split(':')
                slice_list = []
                slice_kwargs = {}
                for item in slice_value:
                    if item:
                        slice_list.append(int(item))
                    else:
                        slice_list.append(None)
                if len(slice_list) == 1:
                    # 取指定下标的值
                    index = slice_list[0]
                    slice_kwargs = {'index': index}
                elif len(slice_list) > 1:
                    # 取指定下标范围内的值
                    key = ['start', 'end', 'step']
                    slice_kwargs = dict(zip(key, slice_list))
                    if hasattr(slice_kwargs, 'step'):
                        slice_kwargs['step'] = slice_kwargs['step'] or 1
                replaced_value = slice(replaced_value, **slice_kwargs)

            if not isinstance(replaced_value, str):
                if content.find(f'"{replaced_key}"') != -1:
                    replaced_key = f'"{replaced_key}"'
                replaced_value = json.dumps(replaced_value, ensure_ascii=False)
            logger.info(f'变量 {temp_replaced_key} 的解析结果为：{replaced_value}')
            content = content.replace(replaced_key, replaced_value)
        return content

    content = parse_variables(variable_regex_compile1, content, http_session_context)
    content = parse_variables(variable_regex_compile2, content, http_session_context)
    return content


def regex_replace_functions(content):
    """
    查询content中符合function_regex_compile正则表达式的方法字符串，
    再使用eval方法得到方法字符串的返回值，
    再将方法返回值替换掉content中的方法字符串
    Args:
        content:

    Returns:

    """
    content = content if isinstance(content, str) else json.dumps(content, ensure_ascii=False)
    while function_regex_compile.search(content):
        replaced_key = temp_replaced_key = function_regex_compile.search(content).group(0)
        logger.info(f'正在解析方法：{temp_replaced_key}')
        fun = function_regex_compile.search(content).group(1)
        replaced_value = eval(fun)

        if not isinstance(replaced_value, str):
            if content.find(f'"{replaced_key}"') != -1:
                replaced_key = f'"{replaced_key}"'
            replaced_value = json.dumps(replaced_value, ensure_ascii=False)
        logger.info(f'方法 {temp_replaced_key} 的运行结果为：{replaced_value}')
        content = content.replace(replaced_key, replaced_value)
    return content


if __name__ == '__main__':
    variables_mapping1 = {"result": {
        "name": "leon",
        "age": 18,
        "ages": [18, 19, 20],
        "hobby": ["book", "bike"],
        "var": [1, 2, 3, 4, 5, 6, 7, 8, 9],
        'name1': [
            {'id': [1]},
            {'id': [2]},
            {'id': [3]}
        ]
    }}

    variable_list = [{
        "source": "result",
        "variable_name": "var",
        "extractor": "JSONPATH",
        "expression": "$.var[1:]",
    }, {
        "source": "result",
        "variable_name": "id",
        "extractor": "JSONPATH",
        "expression": "$..id[0]",
    }, {
        "source": "result",
        "variable_name": "age",
        "extractor": "JSONPATH",
        "expression": "$..age",
    }]
    result = Extractor.extract_value(variables_mapping1, variable_list)
    print(result)
