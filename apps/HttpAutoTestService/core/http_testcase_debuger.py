# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         http_testcase_debuger.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019/11/27 11:38
# -------------------------------------------------------------------------------
import logging
import json
import time

from django.utils import timezone

from apps.HttpAutoTestService.core.http_client import HttpSession
from apps.HttpAutoTestService.core.http_session_context import HttpSessionContext
from apps.HttpAutoTestService.core.utils import regex_parse_args, parse_output_result, \
    parse_http_testcase_validate_result

logger = logging.getLogger(__name__)


class HttpTestcaseDebuger(object):

    def __init__(self, http_session: HttpSession = None, http_session_context: HttpSessionContext = None, name=None,
                 url=None, method=None, cookies=None, headers=None, request_data_type=None, json_data=None,
                 form_data=None, expect_result=None):
        """
        实例化用例调试
        Args:
            http_session:
            http_session_context:
            name:
            url:
            method:
            cookies:
            headers:
            request_data_type:
            json_data: json请求体内容
            form_data: form表单请求体内容
            expect_result:
        """
        self.http_session = http_session
        self.http_session_context = http_session_context
        self.name = name
        self.url = url
        self.method = method
        self.cookies = cookies or {}
        self.headers = headers or {}
        self.request_data_type = request_data_type
        self.json_data = json_data or {}
        self.form_data = form_data or {}
        self.expect_result = expect_result or {}

    def debug(self):
        logger.info(f'=============== 调试用例:【{self.name}】===============')
        # 执行时间
        execute_time = timezone.now()
        # 开始时间，毫秒
        start_ms = time.time() * 1000
        testcase_result = {
            "url": self.url,
            "method": self.method,
            "headers": self.headers,
            "request_data": self.json_data or self.form_data
        }
        variables_mapping = {**self.http_session_context.session_variables_mapping,
                             **self.http_session_context.output_variables_mapping}
        logger.info(f'测试用例【{self.name}】执行前，全局参数内容：{variables_mapping}')

        # 解析接口测试用例参数
        request_arg = None  # 标记正在解析的参数
        try:
            request_arg = "请求URL"
            self.url = regex_parse_args(content=self.url, http_session_context=self.http_session_context)
            logger.info('请求URL：{url}'.format(url=self.url))
            logger.info('请求方法：{method}'.format(method=self.method))

            if not isinstance(self.cookies, dict):
                self.cookies = {}

            request_arg = "Headers参数"
            self.headers = regex_parse_args(content=self.headers, http_session_context=self.http_session_context)
            self.headers = json.loads(self.headers)
            logger.info('请求头信息：{headers}'.format(headers=self.headers))

            request_arg = "Body参数"
            self.form_data = regex_parse_args(content=self.form_data, http_session_context=self.http_session_context)
            self.form_data = json.loads(self.form_data)

            self.json_data = regex_parse_args(content=self.json_data, http_session_context=self.http_session_context)
            self.json_data = json.loads(self.json_data)
            logger.info('请求体参数类型：{request_data_type}'.format(request_data_type=self.request_data_type))
            logger.info('请求体参数：{request_data}'.format(request_data=self.form_data or self.json_data))

            # 期望结果
            request_arg = "期望结果"
            self.expect_result = regex_parse_args(content=self.expect_result,
                                                  http_session_context=self.http_session_context)
            self.expect_result = json.loads(self.expect_result)
            logger.info('期望结果：{expect_result}'.format(expect_result=self.expect_result))
        except Exception as e:
            testcase_result.update({
                "actual_status_code": 0,
                "actual_response_data": "",
                "execute_time": execute_time,
                "elapsed_ms": 0,
                "elapsed": 0,
                "status": "FAIL",
                "failure_reason": f"解析接口测试用例参数【{request_arg}】异常，异常原因：{repr(e)}",
                "output_result": "{}"
            })
            return testcase_result

        request_kwargs = dict(
            cookies=self.cookies,
            headers=self.headers,
            data=self.form_data,
            json=self.json_data
        )

        resp_obj = self.http_session.request(method=self.method, url=self.url, **request_kwargs)
        # 获取HttpSession对象request方法返回数据的keys
        response_keys = resp_obj["response"].keys()
        # 获取请求返回数据实际存储的key
        actual_response_key = list(set(response_keys).intersection(['results', 'text', 'content']))[0]
        actual_response_data = resp_obj["response"][actual_response_key]
        logger.info(f'响应结果：{actual_response_data}')

        if resp_obj["response"]["cookies"]:
            # 获取响应cookies
            cookies = resp_obj["response"]["cookies"].get_dict()
            # 将cookies更新到session中
            self.http_session.cookies.update(cookies)

        # 存储提取输出的变量值
        output_result = parse_output_result(name=self.name, result=resp_obj["response"],
                                            expect_result=self.expect_result,
                                            http_session_context=self.http_session_context)
        # 校验测试用例执行结果
        validate_pass, failure_reason = parse_http_testcase_validate_result(
            result=resp_obj["response"],
            expect_result=self.expect_result,
            http_session_context=self.http_session_context)
        # 结束时间，毫秒
        end_ms = time.time() * 1000
        # 执行耗时，毫秒
        elapsed_ms = round(end_ms - start_ms, 3)
        testcase_result = {
            "url": self.url,
            "method": self.method,
            "headers": self.headers,
            "request_data": self.json_data or self.form_data,
            "actual_status_code": resp_obj["response"]["status_code"],
            "actual_response_data": actual_response_data,
            "execute_time": execute_time,
            "elapsed_ms": elapsed_ms,
            "elapsed": resp_obj["response"]["elapsed"],
            "status": validate_pass,
            "output_result": output_result if output_result else "",
            "failure_reason": failure_reason if validate_pass == "FAIL" else "",
        }
        if validate_pass == 'FAIL':
            logger.warning('用例调试未通过；原因：{failure_reason}'.format(failure_reason=failure_reason))

        return testcase_result
