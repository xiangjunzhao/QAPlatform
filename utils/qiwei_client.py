# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         qiwei_client.py
# Description:
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2022/8/18 12:05
# -------------------------------------------------------------------------------
import logging

import requests

logger = logging.getLogger(__name__)


class QiWeiClient(object):
    """
    企微客户端
    开发者文档：https://developer.work.weixin.qq.com/document/path/91770
    """

    def __init__(self, webhook=None):
        self.webhook = webhook
        self.headers = {'Content-Type': 'application/json'}

    def post(self, data):
        response = requests.post(self.webhook, headers=self.headers, json=data)
        result = response.json()
        return result

    def send_markdown(self, content):
        data = {
            "msgtype": "markdown",
            "markdown": {
                "content": content
            },
        }
        logger.info('发送MarkDown消息：{}'.format(data))
        self.post(data)
