# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         zip_client.py
# Description:  解压缩工具
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/8/4 10:58
# -------------------------------------------------------------------------------
import logging
import os
import zipfile

from utils.common import get_files_of_dir

logger = logging.getLogger(__name__)


def zip_files_or_dirs(files=None, dirs=None, target_file=None):
    """
    压缩文件或目录
    Args:
        files: 待压缩文件
        dirs: 待压缩目录
        target_file: 压缩后文件

    Returns:

    """

    if all([files, isinstance(files, str)]) and all([os.path.exists(files), os.path.isfile(files)]):
        files = [files]
    elif files and isinstance(files, list):
        files = [item for item in files if all([os.path.exists(item), os.path.isfile(item)])]
    else:
        files = []

    logger.info('待压缩文件:{}'.format(''.join(files)))

    if all([dirs, isinstance(dirs, str)]) and all([os.path.exists(dirs), os.path.isdir(dirs)]):
        dirs = [dirs]
    elif dirs and isinstance(dirs, list):
        dirs = [item for item in dirs if all([os.path.exists(item), os.path.isdir(item)])]
    else:
        dirs = []

    logger.info('待压缩目录:{}'.format(''.join(dirs)))

    if not target_file:
        logger.error('压缩文件或目录时，文件名不能为空！！！')
        raise ValueError('压缩文件或目录时，文件名不能为空！！！')

    with zipfile.ZipFile(file=target_file, mode='w', compression=zipfile.ZIP_DEFLATED) as zip_obj:
        # 压缩指定文件
        for file in files:
            file_name = os.path.basename(file)
            zip_obj.write(filename=file, arcname=file_name)

        # 压缩指定目录
        for item in dirs:
            base_dir = os.path.dirname(item)
            for cur_dir, sub_dirs, files in os.walk(item):
                for file in files:
                    filename = os.path.join(cur_dir, file)
                    arcname = filename.replace(base_dir, '')
                    zip_obj.write(filename=filename, arcname=arcname)


def unzip_file(zip_file, target_dir):
    """
    解压zip压缩文件
    Args:
        zip_file: 待解压的zip压缩文件
        target_dir: 解压后的文件存放目录

    Returns:

    """
    # 判断待解压的文件zip_file是否真实存在
    if zip_file and os.path.exists(zip_file) and os.path.isfile(zip_file):
        # 判断目标目录target_dir是否真实存在
        if not target_dir or not os.path.exists(target_dir) or not os.path.isdir(target_dir):
            os.makedirs(target_dir)

        with zipfile.ZipFile(file=zip_file, mode='r', compression=zipfile.ZIP_DEFLATED) as zip_obj:
            zip_obj.extractall(path=target_dir)
    else:
        logger.error('待解压的zip文件不存在,文件名:{}'.format(zip_file))
        raise FileNotFoundError('待解压的zip文件不存在,文件名:{}'.format(zip_file))


if __name__ == '__main__':
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__name__)))
    utils_dir = os.path.join(base_dir, 'utils')
    files = get_files_of_dir(dir_path=utils_dir)
    logs_dir = os.path.join(base_dir, 'logs')
    reports_dir = os.path.join(base_dir, 'reports')
    target_file = os.path.join(reports_dir, 'reports.zip')

    target_dir = os.path.join(base_dir, 'reports1')
    # zip_files_or_dirs(files=files, dirs=logs_dir, target_file=target_file)

    unzip_file(zip_file=target_file, target_dir=target_dir)
