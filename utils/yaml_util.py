# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         yaml_util.py
# Description:  yaml工具
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2022/9/2 14:54
# -------------------------------------------------------------------------------
import os.path

import yaml


class YamlUtil(object):
    @classmethod
    def load_content(cls, path: str):
        if os.path.isfile(path):
            with open(path, 'r', encoding='utf8') as fp:
                data = yaml.safe_load(fp)
                return data
        else:
            raise FileNotFoundError('文件未找到')

    @classmethod
    def dump_content(cls, path: str, content):
        with open(path, 'w', encoding='utf8') as fp:
            yaml.dump(content, fp)
