# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         common.py
# Description:  通用工具
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/8/4 10:42
# -------------------------------------------------------------------------------
import decimal
import json
import os
import time
from datetime import datetime, date


def get_files_of_dir(dir_path, file_type=None, recurisive=False, files=None):
    """
    获取指定目录下的文件
    Args:
        dir_path: 指定目录
        file_type: 指定文件的扩展名
        recurisive: 递归查询所有文件
        files: 存放文件的列表

    Returns: 指定目录下的所有文件列表files

    """

    if file_type is None:
        file_type = list()
    if files is None:
        files = list()

    for file in os.listdir(path=dir_path):
        file_path = os.path.join(dir_path, file)
        if os.path.isfile(path=file_path):  # 判断路径是否是文件
            if file_type and isinstance(file_type, list):
                # 获取指定后缀名的文件
                if os.path.splitext(file_path)[1] in file_type:
                    files.append(file_path)
            else:
                # 获取所有的文件
                files.append(file_path)
        else:
            if recurisive:
                # 递归查询子路径下的文件
                get_files_of_dir(dir_path=file_path, file_type=file_type, recurisive=recurisive, files=files)
    return files


def get_version():
    """
    获取毫秒时间戳字符串，长度为13位
    Returns:

    """
    return str(int(time.time() * 1000))


class CJsonEncoder(json.JSONEncoder):
    """
    Json对日期、Decimal的序列化
    """

    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, date):
            return obj.strftime('%Y-%m-%d')
        elif isinstance(obj, decimal.Decimal):
            return float(obj)
        else:
            return json.JSONEncoder.default(self, obj)
