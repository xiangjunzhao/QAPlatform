# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         config_parser_util.py
# Description:  ini配置文件解析工具
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/8/25 10:02
# -------------------------------------------------------------------------------
import os
from configparser import ConfigParser


class ConfigParserUtil(object):
    def __init__(self, filenames=None):
        """
        ConfigParser工具类初始化方法
        Args:
            filenames: 配置文件
        """
        if not filenames:
            base_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config')
            filenames = os.path.join(base_dir, 'config.ini')
        self.config_parser = ConfigParser(allow_no_value=True)
        self.config_parser.read(filenames=filenames, encoding='utf-8')

    def get_option(self, section, option):
        """
        Get an option value for a given section.
        Args:
            section:
            option:

        Returns:

        """
        return self.config_parser.get(section=section, option=option)

    def get_default(self, option):
        """
        获取DEFAULT的子项
        Args:
            option:

        Returns:

        """
        return self.config_parser.get(section='DEFAULT', option=option)


if __name__ == '__main__':
    base_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config')
    filenames = os.path.join(base_dir, 'config.ini')
    print(ConfigParserUtil(filenames=filenames).get_default('database'))
    print(ConfigParserUtil().get_default('database'))
