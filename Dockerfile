FROM python:3.8.13
WORKDIR /data/QAPlatform
COPY . .
RUN pip3 install -i https://mirrors.aliyun.com/pypi/simple -r requirements.txt \
&& python manage.py makemigrations \
&& python manage.py migrate \
&& chmod +x start.sh
EXPOSE 8000
ENTRYPOINT ["/bin/bash", "start.sh"]