"""
Django settings for QAPlatform project.

Generated by 'django-admin startproject' using Django 2.2.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
import datetime
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'n@r(yy!!jo+^44sq7=2m!q_0w2wxo4udlz6r5a50!r!ajg)vhh'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# 允许访问的主机地址
ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'django_filters',
    'django_celery_beat',
    'django_celery_results',
    'apps.SystemService',
    'apps.BasicAuthService',
    'apps.CeleryScheduledTaskService',
    'apps.HttpAutoTestService',
    'apps.MockService'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'QAPlatform.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['dist'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'QAPlatform.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

# 数据库配置
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "QAPlatform",
        "HOST": "127.0.0.1",
        "PORT": "3306",
        "USER": "root",
        "PASSWORD": "root",
        # 添加以下代码,取消外键检查；
        # 以解决ORM错误:django.db.utils.IntegrityError: (1452, 'Cannot add or update a child row: a foreign key constraint fails
        'OPTIONS': {
            "init_command": "SET foreign_key_checks = 0;",
        }
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

# 设为汉语
LANGUAGE_CODE = 'zh-hans'

# 设置时区亚洲上海
TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = [
    BASE_DIR / 'static',
    BASE_DIR / 'dist/static',
]

# 用户认证MODEL
AUTH_USER_MODEL = 'BasicAuthService.User'
# 用户默认密码
DEFAULT_PASSWD = '123456'
# 用户认证BACKEND
AUTHENTICATION_BACKENDS = [
    'apps.BasicAuthService.backends.UserAuthBackend',
]
EMAIL_BACKEND = 'apps.SystemService.backends.CustomEmailBackend'

# Django_REST_framework配置
REST_FRAMEWORK = {
    # API接口文档SCJEMA CLASS
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.AutoSchema',
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication'
    ],
    # 查看API接口文档时，需注释：DEFAULT_PERMISSION_CLASSES
    'DEFAULT_PERMISSION_CLASSES': ['rest_framework.permissions.IsAuthenticated'],
    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend'],
    'DEFAULT_PAGINATION_CLASS': 'utils.pagination.ResultsSetPagination',
}

# JsonWebToken配置
SIMPLE_JWT = {
    # 指定access_token有效期
    'ACCESS_TOKEN_LIFETIME': datetime.timedelta(days=1),
    # 指定refresh_token有效期
    'REFRESH_TOKEN_LIFETIME': datetime.timedelta(days=30),
    # 设为True时，如果使用TokenRefreshView刷新refresh_token时，将返回新的refresh_token和access_token
    'ROTATE_REFRESH_TOKENS': False,
    'BLACKLIST_AFTER_ROTATION': True,

    # 算法，用于对token执行签名/验证操作
    'ALGORITHM': 'HS256',
    # SIGNING_KEY签名密钥，将用作签名密钥和验证密钥
    'SIGNING_KEY': SECRET_KEY,
    'VERIFYING_KEY': None,

    'AUTH_HEADER_TYPES': ('Bearer',),
    # 来自用户模型的数据库字段，该字段将包含在生成的标记中以标识用户。
    'USER_ID_FIELD': 'id',
    # 生成的令牌中的声明将用于存储用户标识符。
    'USER_ID_CLAIM': 'user_id',

    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    'TOKEN_TYPE_CLAIM': 'token_type',

    'JTI_CLAIM': 'jti',

    # 滑动令牌
    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',
    'SLIDING_TOKEN_LIFETIME': datetime.timedelta(days=1),
    'SLIDING_TOKEN_REFRESH_LIFETIME': datetime.timedelta(days=30),
}

# Celery定时任务配置
# 不使用国际标准时间
CELERY_ENABLE_UTC = False
# 使用亚洲/上海时区
CELERY_TIMEZONE = 'Asia/Shanghai'
# 解决时区问题
CELERY_BEAT_TZ_AWARE = False
# Broker配置，使用Redis作为消息中间件
CELERY_BROKER_URL = 'redis://127.0.0.1:6379/0'
# 使用redis作为中间件
CELERY_BROKER_TRANSPORT = 'redis'
# BACKEND配置，使用数据库作为结果存储[redis、数据库二选一]
# CELERY_RESULT_BACKEND = 'django-db'
# BACKEND配置，使用Redis作为结果存储[redis、数据库二选一，本配置选择redis作为任务结果存储]
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379/1'
# 设置任务接收的序列化类型
CELERY_ACCEPT_CONTENT = ['application/json']
# 设置任务序列化方式
CELERY_TASK_SERIALIZER = 'json'
# 设置任务结果序列化方式
CELERY_RESULT_SERIALIZER = 'json'
# 任务结果过期时间
CELERY_TASK_RESULT_EXPIRES = 24 * 60 * 60
# 配置定时器模块，定时器信息存储在数据库中
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers.DatabaseScheduler'
# celery worker 每次去redis取任务的数量
CELERYD_PREFETCH_MULTIPLIER = 4
# 每个worker执行了多少任务就会死掉
CELERYD_MAX_TASKS_PER_CHILD = 200

# 日志配置
BASE_LOG_DIR = os.path.join(BASE_DIR, "logs")
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,  # 禁用已经存在的logger实例
    # 日志文件的格式
    'formatters': {
        # 详细的日志格式
        'standard': {
            'format': '[%(asctime)s][%(levelname)s][%(name)s.%(filename)s:%(lineno)d][%(message)s]'
        },
        # 简单的日志格式
        'simple': {
            'format': '[%(asctime)s][%(levelname)s][%(name)s.%(filename)s:%(lineno)d][%(message)s]'
        },
        # 定义一个特殊的日志格式
        'collect': {
            'format': '[%(asctime)s][%(levelname)s][%(name)s.%(filename)s:%(lineno)d][%(message)s]'
        }
    },
    # 过滤器
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    # 处理器
    'handlers': {
        # 在终端打印
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],  # 只有在Django debug为True时才在屏幕打印日志
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        # 默认的
        'default': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',  # 保存到文件，自动切
            'filename': os.path.join(BASE_LOG_DIR, "QAPlatform_info.log"),  # 日志文件
            'maxBytes': 1024 * 1024 * 50,  # 日志大小 50M
            'backupCount': 3,  # 最多备份几个
            'formatter': 'standard',
            'encoding': 'utf-8',
        },
        # 专门用来记警告日志
        'warning': {
            'level': 'WARNING',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, "QAPlatform_warning.log"),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 5,
            'formatter': 'standard',
            'encoding': 'utf-8',
        },
        # 专门用来记错误日志
        'error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, "QAPlatform_error.log"),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 5,
            'formatter': 'standard',
            'encoding': 'utf-8',
        }
    },
    'loggers': {
        # 默认的logger应用如下配置
        '': {
            'handlers': ['console', 'default', 'warning', 'error'],  # 上线之后可以把'console'移除
            'level': 'INFO',
            'propagate': True,  # 向更高级别的logger传递
        }
    }
}
