# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         celery
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2019-11-03
# -------------------------------------------------------------------------------

from __future__ import absolute_import, unicode_literals
import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'QAPlatform.settings')  # 设置django环境
app = Celery('CeleryScheduledTaskService')
app.config_from_object('django.conf:settings', namespace='CELERY')  # 使用CELERY_ 作为前缀，在settings中写配置
app.autodiscover_tasks()
