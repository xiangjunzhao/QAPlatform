"""QAPlatform URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include, re_path
from django.conf.urls import url
from django.views.generic import TemplateView, RedirectView
from rest_framework.documentation import include_docs_urls
from apps.MockService.views.mock import UrlMockAPIView

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="index.html")),
    url(r'^favicon.ico$', RedirectView.as_view(url=r'static/favicon.ico')),
    path('QAPlatform/docs/', include_docs_urls(title='API接口文档')),
    # 查看API接口文档时，需注释：re_path(r'^QAPlatform/mock/(.*)', UrlMockAPIView.as_view())
    re_path(r'^QAPlatform/mock/(.*)', UrlMockAPIView.as_view()),
    path('QAPlatform/api/', include('apps.SystemService.urls')),
    path('QAPlatform/api/', include('apps.BasicAuthService.urls')),
    path('QAPlatform/api/', include('apps.CeleryScheduledTaskService.urls')),
    path('QAPlatform/api/', include('apps.HttpAutoTestService.urls')),
    path('QAPlatform/api/', include('apps.MockService.urls')),
]
