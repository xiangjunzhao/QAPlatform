# coding: utf-8

# -------------------------------------------------------------------------------
# Name:         mixins.py
# Description:  
# Author:       XiangjunZhao
# EMAIL:        2419352654@qq.com
# Date:         2020/3/2 14:09
# -------------------------------------------------------------------------------
from rest_framework.mixins import CreateModelMixin


class CustomCreateModelMixin(CreateModelMixin):
    """
    Create a model instance.
    """
    def perform_create(self, serializer):
        instance = serializer.save()
        instance.creator = self.request.user
        instance.save()
