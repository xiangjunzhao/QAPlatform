#! /bin/bash

#启动定时服务celery
nohup celery worker -A QAPlatform --loglevel=info --pool=solo --pidfile=celery-worker.pid --logfile=./logs/celery-worker.log &

#启动定时调度服务django_celery_beat
nohup celery beat -A QAPlatform --loglevel=info --scheduler django_celery_beat.schedulers:DatabaseScheduler --pidfile=celery-beat.pid --logfile=./logs/celery-beat.log &

#启动uwsgi
uwsgi --ini uwsgi.ini