# QAPlatform

### 1、介绍
* 该系统是HTTP接口自动化测试平台，采用前后端分离开发，QAPlatform是该系统的后端程序，QAPlatformWed是该系统的前端程序。作者已将前端程序进行了打包，dist目录即为前端打包程序。  
  
  
* 友情告知：本项目只可用于学习交流使用，不可用于商业用途；已申请著作权，著作权作者所有  
  
  
* 前端代码仓库（持续迭代中……）[https://gitee.com/xiangjunzhao/QAPlatformWeb](https://gitee.com/xiangjunzhao/QAPlatformWeb)  
* 操作手册地址（持续更新中……）[https://gitee.com/xiangjunzhao/QAPlatformDocs](https://gitee.com/xiangjunzhao/QAPlatformDocs)  
* 部署文档地址 [https://pnjhdawrzb.feishu.cn/docs/doccnw9CA3O9yRsvC2q6piHGd2f](https://pnjhdawrzb.feishu.cn/docs/doccnw9CA3O9yRsvC2q6piHGd2f)  
* 部署视频地址（B站）[https://b23.tv/exyogoH](https://b23.tv/exyogoH)  
* 在线体验地址
    * 地址：[https://qaplatform.cn](https://qaplatform.cn)
    * 账号：tester
    * 密码：123456
* 自行搭建平台，登录信息
    * 账号：13612345678  
    * 密码：123456
  

* 项目参与者：三哥
    
* 如果对您有帮助，请点亮 小星星 以表支持，谢谢

![小星星](images/点亮小星星.jpeg)

### 2、软件架构
QAPlatform 使用Python3进行开发，主要选用了 django+django rest framework(简称:drf)+pymysql+redis+celery+uwsgi 框架组合。  
- django+drf：提供restful风格的API接口服务
- pymsql：提供mysql数据库持久化功能
- redis：提供数据缓存功能
- celery：提供定时任务及异步任务功能
- requests：提供http接口测试功能

### 3、部署教程
本次部署环境为CentOS7操作系统。Windows操作系统，请参阅README.windows.md文档。  
本项目采用docker+mysql+redis+nginx+uwsgi进行部署；其中redis、mysql、nginx使用了docker部署，用户可以使用自己熟悉的方式安装redis、mysql、nginx。

#### 3.1、CentOS7安装docker
```
yum install -y epel-release docker-io
```

#### 3.2、docker安装redis
```
docker run -d --name redis -p 6379:6379 --restart=always docker.io/redis:latest
```

#### 3.3、docker安装mysql
```
安装mysql数据库
docker run -d --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root --restart=always -v /var/lib/mysql:/var/lib/mysql docker.io/mysql:5.7.28
```

#### 3.4、docker安装nginx，部署前端程序
```
docker run -d --name nginx -p 80:80 --restart=always docker.io/nginx

# 注意：结合部署环境的情况，修改nginx配置如下
# 1、修改QAPlatform部署的服务器IP以及服务监听的端口
# 示例：系统部署的服务器ip为172.17.0.1；uwsgi.ini设置的程序端口为8000，那么upstream QAPlatform配置如下代码所示
upstream QAPlatform {
    # ip使用部署服务器的ip或域名，端口与uwsgi.ini设置的端口保持一致
    server 172.17.0.1:8000;
}

# 2、修改nginx监听的端口，默认监听的是80端口
# 示例：nginx监听的端口为80，那么server listen配置如下代码所示
server {
    listen 80;
}

# 3、将 nginx配置文件 目录下的nginx-socket.conf更名为nginx.conf；再将nginx.conf拷贝到nginx容器的/etc/nginx目录中
docker cp ./nginx配置文件/nginx.conf nginx:/etc/nginx

# 4、将已打包的前端程序dist目录中的文件拷贝到nginx容器的/home/QAPlatformWeb目录中
docker cp ./dist nginx:/home/QAPlatformWeb

# 5、将nginx服务重新加载
docker exec nginx nginx -s reload
```

#### 3.5、安装Python、Pip环境，创建虚拟环境  
```
一、安装Python环境
# 1、安装依赖
sudo yum install -y libffi-devel zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel zlib gcc make

# 2、下载Python程序
# Python包地址：https://www.python.org/ftp/python/
wget https://www.python.org/ftp/python/3.8.13/Python-3.8.13.tgz

# 3、解压Python-3.8.13.tgz
tar -zxvf Python-3.8.13.tgz

# 4、编译安装
sudo mkdir /usr/local/python3.8.13
cd Python-3.8.13
sudo ./configure --prefix=/usr/local/python3.8.13
sudo make && sudo make install

# 5、建立软链接
sudo ln -s /usr/local/python3.8.13/bin/python3 /usr/bin/python3
sudo ln -s /usr/local/python3.8.13/bin/pip3 /usr/bin/pip3

# 6、验证安装
python3 -V
pip3 -V

二、安装虚拟环境、创建虚拟环境
# 1、安装虚拟环境virtualenv
yum install -y python-virtualenv

# 2、创建虚拟环境
# 在项目根目录创建虚拟环境
virtualenv -p python3 venv

# 3、激活虚拟环境
source ./venv/bin/activate

# 4、退出虚拟环境
deactivate
```

#### 3.6、安装项目依赖
```
# 在激活的虚拟环境中安装项目依赖
# 在requirements.txt所在目录中执行以下命令
pip install -r requirements.txt
```

#### 3.7、迁移数据库，导入初始数据
```
# 创建数据库
# 1、进入mysql docker容器
docker exec -it mysql /bin/bash
# 2、登录mysql数据库
mysql -uroot -proot
# 3、创建QAPlatform数据库
CREATE DATABASE IF NOT EXISTS QAPlatform DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
# 注：本项目的数据库名为：QAPlatform，用户名为：root，密码为：root；用户可根据实际需求，创建相应的数据库以及用户名和密码，并且在项目的QAPlatform/settings.py文件中修改数据库配置，保持一致即可。

# 执行迁移数据库，在manage.py所在目录执行以下命令
python manage.py makemigrations
python manage.py migrate

# 数据库迁移成功生成表后，执行 sql 文件夹中的脚本，导入初始数据
# 执行顺序：basic_user.sql、basic_role.sql、basic_user_roles.sql、basic_markdown.sql、basic_permission.sql
```

### 4、使用说明
#### 4.1、启动、停止uwsgi服务
```
# 启动服务，参数-d代表后台运行
uwsgi -d --ini uwsgi.ini

# 重新加载服务
uwsgi --reload uwsgi.pid

# 停止服务
uwsgi --stop uwsgi.pid
```

#### 4.2、启动定时服务celery
```
# 在项目QAPlatform目录中执行以下命令，启动celery定时服务
nohup celery worker -A QAPlatform --loglevel=info --pool=solo --pidfile=celery-worker.pid --logfile=./logs/celery-worker.log &
```

#### 4.3、启动定时调度服务django_celery_beat
```
# 在项目QAPlatform目录中执行以下命令，启动celery定时调度服务
nohup celery beat -A QAPlatform --loglevel=info --scheduler django_celery_beat.schedulers:DatabaseScheduler --pidfile=celery-beat.pid --logfile=./logs/celery-beat.log &
```

### 5、前端界面展示
#### 5.1、定时任务服务
- 5.1.1、Crontab日程列表  
![Crontab日程列表](./images/CeleryScheduledTaskService/cron/Crontab日程列表.jpg)
- 5.1.2、添加Crontab  
![添加Crontab](./images/CeleryScheduledTaskService/cron/添加Crontab.jpg)
- 5.1.3、定时任务列表  
![定时任务列表](./images/CeleryScheduledTaskService/scheduledtask/定时任务列表.jpg)
- 5.1.4、添加定时任务  
![添加定时任务](./images/CeleryScheduledTaskService/scheduledtask/添加定时任务.jpg)

#### 5.2、接口自动化测试
- 5.2.1、环境列表  
![环境列表](./images/HttpAutoTestService/environment/环境列表.jpg)

- 5.2.2、创建环境  
![创建环境](./images/HttpAutoTestService/environment/创建环境.jpg)

- 5.2.3、项目列表  
![项目列表](./images/HttpAutoTestService/project/项目列表.jpg)
- 5.2.4、编辑项目  
![编辑项目](./images/HttpAutoTestService/project/编辑项目.jpg)

- 5.2.5、模块列表  
![模块列表](./images/HttpAutoTestService/module/模块列表.jpg)

- 5.2.6、接口列表  
![接口列表](./images/HttpAutoTestService/api/接口列表.jpg) 
- 5.2.7、创建接口—基础信息  
![创建接口—基础信息](./images/HttpAutoTestService/api/创建接口1.jpg)
- 5.2.8、创建接口—Headers参数定义  
![创建接口—Headers参数定义](./images/HttpAutoTestService/api/创建接口2.jpg)
- 5.2.9、创建接口—Body参数定义  
![创建接口—Body参数定义](./images/HttpAutoTestService/api/创建接口3.jpg)
- 5.2.10、创建接口—用例期望结果定义  
![创建接口—用例期望结果定义](./images/HttpAutoTestService/api/创建接口4.jpg)
- 5.2.11、EXCEL导入接口和用例模板  
![EXCEL导入接口和用例模板](./images/HttpAutoTestService/api/导入接口和用例EXCEL模板.jpg)
- 5.2.12、EXCEL导入接口和用例  
![EXCEL导入接口和用例](./images/HttpAutoTestService/api/导入接口和用例.jpg)

- 5.2.13、扩展方法列表  
![扩展方法列表](./images/HttpAutoTestService/extmethods/扩展方法列表.jpg)

- 5.2.14、用例列表  
![用例列表](./images/HttpAutoTestService/testcase/用例列表.jpg)
- 5.2.15、创建用例  
![创建用例](./images/HttpAutoTestService/testcase/创建用例.jpg)
- 5.2.16、导入用例  
![导入用例](images/HttpAutoTestService/testcase/导入用例.jpg)
- 5.2.17、编辑调试用例—基础信息  
![编辑调试用例—基础信息](images/HttpAutoTestService/testcase/编辑调试用例1.jpg)
- 5.2.18、编辑调试用例—Headers参数  
![编辑调试用例—Headers参数](./images/HttpAutoTestService/testcase/编辑调试用例2.jpg)
- 5.2.19、编辑调试用例—Body参数  
![编辑调试用例—Body参数](./images/HttpAutoTestService/testcase/编辑调试用例3.jpg)
- 5.2.20、编辑调试用例—期望结果  
![编辑调试用例—期望结果](./images/HttpAutoTestService/testcase/编辑调试用例4.jpg)
- 5.2.21、编辑调试用例—调试用例  
![编辑调试用例—调试用例](images/HttpAutoTestService/testcase/调试用例.jpg)

- 5.2.22、场景列表  
![场景列表](./images/HttpAutoTestService/testsuite/场景列表.jpg)
- 5.2.23、创建场景—基础信息  
![创建场景—基础信息](./images/HttpAutoTestService/testsuite/创建场景1.jpg)
- 5.2.24、创建场景—全局参数配置  
![创建场景—全局参数配置](./images/HttpAutoTestService/testsuite/创建场景2.jpg)
- 5.2.25、场景组织用例  
![场景组织用例](images/HttpAutoTestService/testsuite/场景组织用例.jpg)

- 5.2.26、场景集列表  
![场景集列表](./images/HttpAutoTestService/testsuiteset/场景集列表.jpg)
- 5.2.27、场景集组织场景  
![场景集组织场景](./images/HttpAutoTestService/testsuiteset/场景集组织场景.jpg)
 
- 5.2.28、测试结果—用例结果列表  
![测试结果—用例结果列表](images/HttpAutoTestService/result/用例结果列表.jpg)
- 5.2.29、测试结果—用例结果详情  
![测试结果—用例结果详情](images/HttpAutoTestService/result/用例结果详情.jpg)
- 5.2.30、测试结果-场景结果列表  
![测试结果-场景结果列表](images/HttpAutoTestService/result/场景结果列表.jpg)
- 5.2.31、测试结果—场景结果详情  
![测试结果—场景结果详情](images/HttpAutoTestService/result/场景结果详情.jpg)

- 5.2.32、测试报表  
![测试报表](images/HttpAutoTestService/report/测试报表.jpg)
- 5.2.33、用例测试报告  
![用例测试报告](images/HttpAutoTestService/report/用例测试报告.jpg)
- 5.2.34、场景测试报告  
![场景测试报告](images/HttpAutoTestService/report/场景测试报告.jpg)

- 5.2.35、帮助文档  
![帮助文档](images/HttpAutoTestService/helpdocs/帮助文档.jpg)
  
### 6、捐赠
#### 如果对您有帮助，请微信扫码，捐赠以表支持，谢谢
![捐赠](images/微信捐赠.jpeg)