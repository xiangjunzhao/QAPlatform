# QAPlatform

### 1、介绍
该系统是HTTP接口自动化测试平台，采用前后端分离开发，QAPlatform是该系统的后端程序，QAPlatformWed是该系统的前端程序。作者已将前端程序进行了打包，dist目录即为前端打包程序。  

### 2、软件架构
QAPlatform 使用Python3进行开发，主要选用了 django+django rest framework(简称:drf)+pymysql+redis+celery+uwsgi 框架组合。  
- django+drf：提供restful风格的API接口服务
- pymsql：提供mysql数据库持久化功能
- redis：提供数据缓存功能
- celery：提供定时任务及异步任务功能

### 3、部署教程
本次部署环境为Windows操作系统，此部署方式适合于个人使用。CentOS7操作系统，请参阅README.md文档。
```
本次采用docker安装mysql、redis，用户也可以选择自己熟悉的方式来安装mysql、redis。
```

#### 3.1、Windows安装docker
```
Windows7、8安装docker
https://www.runoob.com/docker/windows-docker-install.html

Windows10安装docker
https://www.cnblogs.com/wyt007/p/10656813.html
https://blog.csdn.net/zzq060143/article/details/91050272
```

#### 3.2、docker安装redis
```
docker run -d --name redis -p 6379:6379 --restart=always docker.io/redis:latest
```

#### 3.3、docker安装mysql数据库
```
docker run -d --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root --restart=always docker.io/mysql:5.7.28
```

#### 3.5、安装配置Python3、Pip环境
```
# Python安装教程请自行百度
```

#### 3.6、安装项目依赖
```
# 在requirements.txt所在目录中执行以下命令
pip install -r requirements.txt
```

#### 3.7、迁移数据库，导入初始数据
```
# 创建数据库
# 1、进入mysql docker容器
docker exec -it mysql /bin/bash
# 2、登录mysql数据库
mysql -uroot -proot
# 3、创建QAPlatform数据库
CREATE DATABASE IF NOT EXISTS QAPlatform DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
# 注：本项目的数据库名为：QAPlatform，用户名为：root，密码为：root；用户可根据实际需求，创建相应的数据库以及用户名和密码，并且在项目的QAPlatform/settings.py文件中修改数据库配置，保持一致即可。

# 执行迁移数据库，在manage.py所在目录执行以下命令
python manage.py makemigrations
python manage.py migrate

# 数据库迁移成功生成表后，执行 sql 文件夹中的脚本，导入初始数据
# 执行顺序：basic_user.sql、basic_role.sql、basic_user_roles.sql、basic_markdown.sql、basic_permission.sql
```

### 4、使用说明
#### 4.1、运行QAPlatform
```
python manage.py runserver 0.0.0.0:8000 --insecure
程序启动后，在浏览器中输入 http://localhost:8000/ 进行访问
```

#### 4.2、启动定时服务celery
```
# 在项目QAPlatform目录中执行以下命令，启动celery定时服务
celery worker -A QAPlatform --loglevel=info --pool=solo --pidfile=celery-worker.pid --logfile=./logs/celery-worker.log
```

#### 4.3、启动定时调度服务django_celery_beat
```
# 在项目QAPlatform目录中执行以下命令，启动celery定时调度服务
celery beat -A QAPlatform --loglevel=info --scheduler django_celery_beat.schedulers:DatabaseScheduler --pidfile=celery-beat.pid --logfile=./logs/celery-beat.log
```
