INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('070cebaa-fbfc-4b35-8263-44c0907b16d2', '2021-05-13 07:52:21.604819', '2021-05-13 07:52:41.457453', null, 0, 'MD5加密', 'md5_encryption(raw_str, sha_str='''', toupper=False)', 'MD5加密', 'raw_str：需要执行MD5加密的字符串
参数类型：字符串

sha_str：MD5加密的盐值
参数类型：字符串

toupper：是否将加密后的结果转大写
参数类型：布尔值', '经MD5加密后的字符串
返回值类型：字符串', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('085a4f1d-37bc-403c-bb1c-ac511f3b2588', '2021-05-13 08:18:01.528492', '2021-05-13 08:18:01.537494', null, 0, '切片', 'slice(obj, index=None, start=None, end=None, step=1)', '类型于python的切片功能', 'obj：被切片对象
参数类型：字符串、元组、列表

index: 索引
start: 开始索引
end: 结束索引（不含）
step: 步长', '', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('0e0c60c0-de80-48f4-8200-4b59aad79a2b', '2021-05-13 06:51:12.810496', '2021-05-13 06:51:12.815497', null, 0, '暂停', 'sleep(second)', '', '', '', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('0ed24216-bdcc-4af2-8207-6403a56d6009', '2021-05-13 08:00:26.133294', '2021-05-13 08:00:26.139294', null, 0, '随机选择', 'random_choice(object)', '从入参对象object中随机选择一个对象，返回', '', '', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('154d1ba8-f3a3-4f32-9949-54fd2b46458b', '2021-05-13 08:16:03.988211', '2021-05-13 08:16:04.005145', null, 0, '将毫秒转换成 h:m:s.ms格式字符串', 'ms_fmt_hms(ms)', '将毫秒转换成 h:m:s.ms格式字符串', 'ms：毫秒', '毫秒转换成 h:m:s.ms格式后字符串
返回值类型：字符串', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('2b5ffc80-5754-43e7-80f2-3bd6c5a74dc3', '2021-05-13 08:05:48.684354', '2021-05-13 08:05:48.703343', null, 0, '正则表达式提取', 'regex_extract(string, pattern, group=None)', '', 'string：目标字符串
参数类型：字符串

pattern：正则表达式
参数类型：字符串

group：分组组号
参数类型：整数', '正则表达式匹配结果
返回值类型：字符串', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('3520e79b-4718-40bf-86fd-666ba155cc36', '2021-05-13 08:07:27.203351', '2021-05-13 08:07:27.207350', null, 0, '截取字符串（切片）', 'substr(raw_str, start=None, end=None)', '', 'raw_str：原始字符串
参数类型：字符串

start:：字符串开始位置
参数类型：整数

end：字符串结束位置
参数类型：整数', '截取后的字符串
返回值类型：字符串', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('53fb1e16-e1fc-4bfa-90f2-67454c9a73dd', '2021-05-13 08:19:57.054077', '2021-12-27 01:51:52.208674', null, 0, '列表切片', 'sublist(raw_list, start=None, end=None)', '', 'raw_list：原始列表
参数类型：列表

start：开始位置
参数类型：整数

end：结束位置
参数类型：整数', '列表切片
返回值类型：列表', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('58586295-2254-49f3-b8e9-8ceb8c7ccbb0', '2021-07-12 08:08:08.491901', '2021-07-12 08:08:08.496795', null, 1, '测试', 'get_test()', '测试一下', '测试一下', '测试一下', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('62855ab7-31f7-48d3-b5a7-94c29104212f', '2021-05-13 07:49:47.981519', '2021-05-13 07:49:47.985576', null, 0, '获取当前时间指定间隔后的时间', 'get_delta_time(days=0, hours=0, minutes=0, seconds=0, fmt="%Y-%m-%d %H:%M:%S")', '获取当前时间指定间隔后的时间', 'days：距离当前时间多少天
参数类型：浮点数、整数、正数、负数

hours：距离当前时间多少时
参数类型：浮点数、整数、正数、负数

minutes：距离当前时间多少分
参数类型：浮点数、整数、正数、负数

seconds： 距离当前时间多少秒
参数类型：浮点数、整数、正数、负数

fmt：时间格式
参数类型：字符串
默认值："%Y-%m-%d %H:%M:%S"', '获取当前时间指定间隔后的时间，默认格式为：%Y-%m-%d %H:%M:%S', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('72f97c80-9c35-47ce-ba0d-562b0387a5a6', '2021-07-14 07:52:13.854066', '2021-07-14 07:52:13.868020', null, 1, '324', '32', '23', '32', '23', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('76df6bb3-3d7d-4a05-b699-b1ab984a18ae', '2021-05-13 07:38:34.566821', '2021-05-13 07:38:34.570822', null, 0, '列表', 'list(object)', '将入参转化成列表', '', '入参转化后的列表
返回值类型：列表', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('7b4f36ea-d394-4ea9-915f-573780b7b19a', '2021-05-13 07:32:52.395534', '2022-02-12 01:21:46.043032', null, 0, '创建字符串对象', 'str(object)', '将入参转化成字符串', 'object
参数类型：任意', '转化后的字符串对象
返回值类型：字符串对象', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('8483b9e4-69bd-4030-9cf0-97f410994c34', '2021-05-13 07:44:52.606668', '2021-05-13 07:44:52.609669', null, 0, '获取当前日期时间', 'get_current_time()', '获取当前日期时间', '无', '当前日期时间字符串，默认格式为：%Y-%m-%d %H:%M:%S', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('86a76120-d5fb-4177-822e-4df0e8211131', '2021-05-13 07:36:09.569047', '2021-05-13 07:36:09.573048', null, 0, '获取对象长度', 'len(object)', '获取入参对象的长度', 'object
参数类型：字符串、列表、元组、字典等', '对象长度
返回值类型：整型数值', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('95daa692-a882-4fe6-80e4-b36ddae9713d', '2021-05-13 07:43:04.511504', '2021-05-13 07:43:04.515505', null, 0, '获取当前日期', 'get_current_date()', '获取当前日期', '无', '当前日期字符串，默认格式为：%Y-%m-%d', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('972b0037-c1f9-4644-b0c6-252220678a3f', '2021-09-14 09:03:46.515295', '2021-09-14 09:03:46.527706', null, 1, 'eee', 'eee', '', '', '', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('97a8fd24-b56b-44b3-8694-edabf39ee756', '2021-12-29 08:59:04.336045', '2021-12-29 08:59:04.341132', null, 1, 'cehsi', 'erf', '', '', '', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('a0df9026-d6d0-4eac-b87d-7b97eea7ea25', '2021-08-28 16:06:09.276029', '2021-08-28 16:06:09.281964', null, 1, 'sum1', 'sum1', '', '', '', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('b0182222-6a15-4d6a-a76a-ad5e0bfddaf5', '2021-05-13 08:02:00.613369', '2022-06-02 10:36:02.619789', null, 0, '生成随机数', 'gen_random_num(length)', '', 'length：指定生成随机数长度
参数类型：整数', '指定长度的整数
返回值类型：整数', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('b11c3969-e41a-47ea-97f4-92910b655f8a', '2021-07-11 13:50:14.906134', '2021-07-11 13:50:14.929958', null, 1, 'aa', 'aa', 'aaaaaa', 'aa', 'aaa', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('b9740076-f149-42ec-9dbc-381cfb77694e', '2021-05-13 07:57:03.831113', '2021-05-13 07:57:03.847081', null, 0, '将JSON格式的字符串反序列化成JSON对象', 'json_loads(object)', '将JSON格式的字符串反序列化成JSON对象', '', 'JSON格式字符串反序化后的JSON对象
返回值类型：JSON对象', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('ca4924b4-8584-4f7e-9e7f-784ef72e2273', '2021-05-13 07:54:55.303885', '2021-05-13 07:57:50.487568', null, 0, '将对象序列化成JSON格式的字符串', 'json_dumps(object)', '将对象序列化成JSON格式的字符串', 'JSON格式对象', 'JSON格式对象序列化后的字符串
返回值类型：字符串', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('cdbeefaa-16e6-4d6a-ab29-894e4a7d072e', '2021-05-13 08:12:19.351761', '2021-05-13 08:12:19.362791', null, 0, '字符串连接', 'str_join(obj, connector=",")', '', 'obj：被连接对象
参数类型：列表、无组

connector：连接符
参数类型：字符串', '连接后的字符串
返回值类型：字符串', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('cdf807f4-9e77-4ef6-baa9-2736f25472cb', '2021-05-13 07:25:28.114651', '2021-05-13 07:28:09.529303', null, 0, '取整', 'int(object)', '将入参转化为整数', 'object
参数类型：数值型数字、数值型字符串', '', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('f0b0f3fc-913e-40d1-bcf9-e4acba0e9f9b', '2021-05-13 08:14:17.861858', '2022-02-12 01:21:03.050640', null, 0, '获取时间戳', 'get_timestamp(length=13)', '', 'length：指定时间戳字符串长度
参数类型：整数', '指定长度的时间戳
返回值类型：整数', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');
INSERT INTO http_ext_method (id, create_time, update_time, remark, is_deleted, name, ext_method, ext_method_desc, args_desc, returned_value_desc, type, creator_id, modifier_id) VALUES ('f10b7e09-be83-48fc-a63b-07a121684731', '2021-05-13 08:03:09.149476', '2021-06-02 08:50:35.482740', null, 0, '生成随机字符串', 'gen_random_str(length)', '', 'length：指定生成随机数长度
参数类型：整数', '指定长度的字符串
返回值类型：字符串', 'BUILT_IN_KEYWORD', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b', '6ad502a9-f2dd-4e33-ab9a-e702fa4f877b');